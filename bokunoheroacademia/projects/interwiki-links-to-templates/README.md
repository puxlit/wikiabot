# Interwiki Links to Templates

The main goal is to replace interwiki links to Wikipedia and other Wikia communities (for pages in the main namespace) with the templates [Wikipedia](https://bokunoheroacademia.wikia.com/wiki/Template:Wikipedia) and [W](https://bokunoheroacademia.wikia.com/wiki/Template:W) respectively.
Expected forms are `[[wikipedia:]]` for Wikipedia and either `[[w:c:]]` or `[[wikia:c:]]` for Wikia communities; note that prefixes are case insensitive.
Labels should be omitted if redundant.
Support for the [pipe trick](https://en.wikipedia.org/wiki/Help:Pipe_trick) need not be implemented.
Prior to mass editing, links should be canonicalized.

Some example conversions follow.

| From                                                               | To                                                                      |
| ------------------------------------------------------------------ | ----------------------------------------------------------------------- |
| <code>[[wikipedia:Yūki Kaji&#x7c;Yūki Kaji]]</code>                | <code>{{Wikipedia&#x7c;Yūki Kaji}}</code>                               |
| <code>[[Wikipedia:Heterochromia iridum&#x7c;heterochromia]]</code> | <code>{{Wikipedia&#x7c;Heterochromia iridum&#x7c;heterochromia}}</code> |
| <code>[[wikia:c:avatar:Zuko&#x7c;Zuko]]</code>                     | <code>{{W&#x7c;avatar&#x7c;Zuko}}</code>                                |
| <code>[[w:c:shingekinokyojin:Eren Jaeger&#x7c;Eren Jaeger]]</code> | <code>{{W&#x7c;attackontitan&#x7c;Eren Jaeger}}</code>                  |
