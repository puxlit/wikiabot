{{W|akamegakill|Incursio}}
{{W|akira|Akira (Manga)}}
{{W|animevoiceover|Aaron Dismuke}}
{{W|animevoiceover|Alex Organ}} << {{W|voiceacting|Alex Organ}}
{{W|animevoiceover|Charlie Campbell}} << {{W|voiceacting|Charlie Campbell}}
{{W|animevoiceover|Christopher R. Sabat}}
{{W|animevoiceover|Christopher Wehkamp}} << {{W|voiceacting|Christopher Wehkamp}}
{{W|animevoiceover|Clifford Chapin}}
{{W|animevoiceover|Clifford Chapin}} << {{Wikipedia|Clifford Chaplin}}
{{W|animevoiceover|Colleen Clinkenbeard}}
{{W|animevoiceover|Cris George}} << {{Wikipedia|Cris George}}
{{W|animevoiceover|David Matranga}}
{{W|animevoiceover|David Wald}}
{{W|animevoiceover|Felecia Angelle}}
{{W|animevoiceover|Felecia Angelle}} << {{Wikipedia|Felicia Angelle}}
{{W|animevoiceover|Jarrod Greene}}
{{W|animevoiceover|Jessica Cavanagh}} << {{W|voiceacting|Jessica Cavanagh}}
{{W|animevoiceover|Josh Grelle}}
{{W|animevoiceover|Justin Briner}}
{{W|animevoiceover|Justin Briner}} << {{W|voiceacting|Justin Briner}}
{{W|animevoiceover|Kate Oxley}}
{{W|animevoiceover|Kyle Phillips}} << {{W|voiceacting|Kyle Phillips}}
{{W|animevoiceover|Lara Woodhull}}
{{W|animevoiceover|Luci Christian}}
{{W|animevoiceover|Morgan Berry}}
{{W|attackontitan|Eren Jaeger (Anime)}} << {{W|attackontitan|Eren Jaeger}}
{{W|avatar|Zuko}}
{{W|avp|P-5000 Powered Work Loader}} << {{W|avp|Caterpillar P-5000 Work Loader}}
{{W|bleach|Ichigo Kurosaki}}
{{W|community|Community Central}}
{{W|dc|Dick Grayson}}
{{W|dc|Nathaniel Heywood (New Earth)}}
{{W|dc|Superman}}
{{W|dragonball|Bubbles}}
{{W|dragonball|Final Flash}}
{{W|dragonball|King Kai}}
{{W|fairytail|Gajeel Redfox}}
{{W|fairytail|Lyon Vastia}}
{{W|hunterxhunter|Gon Freecss}}
{{W|jojo|Jotaro Kujo}}
{{W|jojo|Stand Cry}}
{{W|jojo|Star Platinum}}
{{W|kekkai-sensen|Klaus Von Reinherz}} << {{W|Kekkai-Sensen|Klaus Von Reinherz}}
{{W|mario|Goomba}} << {{W|mariowiki|goomba}}
{{W|mario|Koopa}} << {{W|mariowiki|koopa (species)}}
{{W|mario|Piranha Plant}} << {{W|mariowiki|Piranha Plant}}
{{W|mario|Thwomp}} << {{W|mariowiki|Thwomp}}
{{W|marvel|Piotr Rasputin (Earth-616)}}
{{W|marvel|Spider-Man}}
{{W|marvel|Steven Rogers (Earth-616)}}
{{W|nanatsu-no-taizai|Meliodas}}
{{W|naruto|Inari}}
{{W|naruto|Naruto Uzumaki}} << {{W|naruto|Naruto}}
{{W|onepiece|One Piece (Manga)}}
{{W|oumagadokizoo|Chapter 0}}
{{W|oumagadokizoo|Hana Aoi}} << {{W|oumagadokizoo|Hana}}
{{W|oumagadokizoo|Hana Aoi}}
{{W|oumagadokizoo|Isana}}
{{W|oumagadokizoo|Kouhei Hirikoshi}} << {{W|oumagadokizoo|Kōhei Horikoshi}}
{{W|oumagadokizoo|Sakamata}}
{{W|oumagadokizoo|Shiina}}
{{W|oumagadokizoo|Shikuma}}
{{W|oumagadokizoo|Shishido}}
{{W|oumagadokizoo|Takahiro}}
{{W|oumagadokizoo|Toytoy}}
{{W|oumagadokizoo|Uwabami}}
{{W|oumagadokizoo|Ōmagadoki Dōbutsuen}}
{{W|powerrangers|Anubis Cruger}}
{{W|powerrangers|Doggie Kruger}}
{{W|powerrangers|Power Rangers S.P.D.}}
{{W|powerrangers|Tokusou Sentai Dekaranger}}
{{W|senseinobulge|Kōhei Horikoshi}}
{{W|senseinobulge|Sensei no Bulge}}
{{W|senseinobulge|Uchū Shōnen Bulge}}
{{W|starwars|Alderaan}}
{{W|starwars|Hoth}}
{{W|starwars|Kashyyyk}}
{{W|starwars|Luke Skywalker}}
{{W|starwars|Star Wars}} << {{W|starwars|''Star Wars''}}
{{W|starwars|Star Wars: Episode IV A New Hope}}
{{W|starwars|Tatooine}}
{{W|starwars|Wookiee}}
{{W|starwars|Yoda}}
{{W|worldtrigger|Chika Amatori}}
{{W|worldtrigger|Ōji Unit}}
{{W|x-men|Cyclops (Scott Summers)}}
{{W|x-men|Wolverine (James "Logan" Howlett)}}
{{W|zelda|Deku}}
{{Wikipedia|Acid}}
{{Wikipedia|Adhesive}}
{{Wikipedia|Adsorption}}
{{Wikipedia|Adult Swim}} << {{Wikipedia| Adult Swim}}
{{Wikipedia|Adult Swim}}
{{Wikipedia|Aichi Prefecture}}
{{Wikipedia|Akame ga Kill!}}
{{Wikipedia|Akeno Watanabe}}
{{Wikipedia|Akihabara}}
{{Wikipedia|Akio Ōtsuka}}
{{Wikipedia|Akita Prefecture}}
{{Wikipedia|Alexis Tipton}}
{{Wikipedia|Alien (film)}}
{{Wikipedia|Alien (franchise)}}
{{Wikipedia|Ami Koshimizu}}
{{Wikipedia|Anastasia Muñoz}}
{{Wikipedia|Anime Expo}}
{{Wikipedia|Anthony Bowling}}
{{Wikipedia|Anti-gravity}}
{{Wikipedia|Aoi Yūki}}
{{Wikipedia|Aomori Prefecture}}
{{Wikipedia|Arcade game}} << {{Wikipedia|Arcade Game}}
{{Wikipedia|Austin Tindle}}
{{Wikipedia|Avengers (comics)}} << {{Wikipedia|Avengers}}
{{Wikipedia|Ayane Sakura}}
{{Wikipedia|Bandai Namco Entertainment}} << {{Wikipedia|Bandai Namco}}
{{Wikipedia|Bandai Namco Entertainment}}
{{Wikipedia|Banpresto}}
{{Wikipedia|Black hole}}
{{Wikipedia|Bleach (TV series)}}
{{Wikipedia|Blood}}
{{Wikipedia|Blood Blockade Battlefront}}
{{Wikipedia|Blood type}}
{{Wikipedia|Booster Gold}}
{{Wikipedia|Boys on the Run}}
{{Wikipedia|Brainwashing}}
{{Wikipedia|Brandon Potter}}
{{Wikipedia|Brina Palencia}}
{{Wikipedia|Bronze Age of Comic Books}}
{{Wikipedia|Bryn Apprill}}
{{Wikipedia|Bubaigawara Station}}
{{Wikipedia|Bubble (physics)}}
{{Wikipedia|Bōjutsu}}
{{Wikipedia|Caitlin Glass}}
{{Wikipedia|Captain America}}
{{Wikipedia|Cassandra Lang}}
{{Wikipedia|Cassandra Lee Morris}}
{{Wikipedia|Cement}}
{{Wikipedia|Cenobite (Hellraiser)}}
{{Wikipedia|Cheongsam}}
{{Wikipedia|Cheongsam}} << {{Wikipedia|Qipao}}
{{Wikipedia|Cherami Leigh}}
{{Wikipedia|Chiba Prefecture}}
{{Wikipedia|China}}
{{Wikipedia|Chris Rager}}
{{Wikipedia|Christopher Bevins}}
{{Wikipedia|Christopher Sabat}}
{{Wikipedia|Chronostasis}}
{{Wikipedia|Chuck Huber}}
{{Wikipedia|Cis AB}}
{{Wikipedia|Clint Eastwood}}
{{Wikipedia|Clone Saga}}
{{Wikipedia|Cloning}}
{{Wikipedia|Close quarters combat}} << {{Wikipedia|Close Quarters Combat}}
{{Wikipedia|Colleen Clinkenbeard}}
{{Wikipedia|Compression (physics)}}
{{Wikipedia|Confession}}
{{Wikipedia|Coordinate system}}
{{Wikipedia|Copyright}} << {{Wikipedia|Copyrights}}
{{Wikipedia|Corrosion}}
{{Wikipedia|Cousin Itt}}
{{Wikipedia|Cyclops (Marvel Comics)}}
{{Wikipedia|Daiki Yamashita}}
{{Wikipedia|Darth Vader}}
{{Wikipedia|David Matranga}}
{{Wikipedia|David Wald}}
{{Wikipedia|Deadpool}}
{{Wikipedia|Decomposition}}
{{Wikipedia|Doping in sport}}
{{Wikipedia|Dragon Ball}}
{{Wikipedia|Dry eye syndrome}}
{{Wikipedia|Dust}}
{{Wikipedia|Earthflow}}
{{Wikipedia|Ectoplasm (paranormal)}}
{{Wikipedia|Ehime Prefecture}}
{{Wikipedia|Elasticity (physics)}}
{{Wikipedia|Electricity}}
{{Wikipedia|Emily Neves}}
{{Wikipedia|Entomophobia}}
{{Wikipedia|Eri Kitamura}}
{{Wikipedia|Eric Vale}}
{{Wikipedia|Etsuko Kozakura}}
{{Wikipedia|Explosion}}
{{Wikipedia|Fair use}}
{{Wikipedia|Fantastic Four}}
{{Wikipedia|Fat}}
{{Wikipedia|Firestorm (comics)}}
{{Wikipedia|Flash (comics)}} << {{Wikipedia|The Flash}}
{{Wikipedia|Flight}}
{{Wikipedia|Ford Torino}}
{{Wikipedia|Four past Midnight}}
{{Wikipedia|France}}
{{Wikipedia|Francis Xavier}}
{{Wikipedia|Frog}}
{{Wikipedia|Fukuoka Prefecture}}
{{Wikipedia|Fukushima Prefecture}}
{{Wikipedia|Garganta}}
{{Wikipedia|George A. Romero}}
{{Wikipedia|Ghost}}
{{Wikipedia|Giants (Greek mythology)#The Gigantomachy}}
{{Wikipedia|Giganta}}
{{Wikipedia|Golem}}
{{Wikipedia|Gran Torino}}
{{Wikipedia|Grappling hook}} << {{Wikipedia|grappling hook}}
{{Wikipedia|Greg Ayres}}
{{Wikipedia|Gunma Prefecture}}
{{Wikipedia|Hakama}} << {{Wikipedia|hakama}}
{{Wikipedia|Hand fan}} << {{Wikipedia|Hand_Fan}}
{{Wikipedia|Hardness}}
{{Wikipedia|Heart}} << {{Wikipedia|heart}}
{{Wikipedia|Hellraiser (franchise)}}
{{Wikipedia|Heterochromia iridum}}
{{Wikipedia|Hikaru Midorikawa}}
{{Wikipedia|Hime cut}}
{{Wikipedia|Hiro Shimono}}
{{Wikipedia|Hiroshima Prefecture}}
{{Wikipedia|Hiroyuki Yoshino (voice actor)}} << {{Wikipedia|Hiroyuki Yoshino}}
{{Wikipedia|Hiroyuki Yoshino (voice actor)}}
{{Wikipedia|Hokkaido}}
{{Wikipedia|Homeroom#Japan}}
{{Wikipedia|Hulk (comics)}}
{{Wikipedia|Hulu}}
{{Wikipedia|Human voice}}
{{Wikipedia|Hunter × Hunter}}
{{Wikipedia|Ian Sinclair (voice actor)}}
{{Wikipedia|Indigenous peoples of the Americas}}
{{Wikipedia|Invincible (comics)#Enemies}}
{{Wikipedia|Iron Man}}
{{Wikipedia|Irresistible force paradox}}
{{Wikipedia|Iwate Prefecture}}
{{Wikipedia|J. Michael Tatum}} << {{Wikipedia|J Michael Tatum}}
{{Wikipedia|J. Michael Tatum}}
{{Wikipedia|Jad Saxton}}
{{Wikipedia|Jamie Madrox}}
{{Wikipedia|Jamie Marchi}}
{{Wikipedia|Japanese armour}} << {{Wikipedia|Japanese armor}}
{{Wikipedia|Japanese school uniform}}
{{Wikipedia|Japanese school uniform#Gakuran}}
{{Wikipedia|Jason Douglas}}
{{Wikipedia|Jason Liebrecht}}
{{Wikipedia|Jeremy Inman}}
{{Wikipedia|Jerry Jewell}} << {{Wikipedia| Jerry Jewell}}
{{Wikipedia|Jiaozi#Gyōza}}
{{Wikipedia|JoJo's Bizarre Adventure}} << {{Wikipedia|Jojo's Bizarre Adventure}}
{{Wikipedia|John Burgmeier}}
{{Wikipedia|John Swasey}}
{{Wikipedia|Josh Grelle}}
{{Wikipedia|Juli Erickson}}
{{Wikipedia|Junichi Suwabe}}
{{Wikipedia|Justin Cook}}
{{Wikipedia|Kagawa Prefecture}}
{{Wikipedia|Kagoshima Prefecture}}
{{Wikipedia|Kaito Ishikawa}}
{{Wikipedia|Kanagawa Prefecture}}
{{Wikipedia|Kanji}}
{{Wikipedia|Kaori Nazuka}}
{{Wikipedia|Katakana}}
{{Wikipedia|Kei Shindō}}
{{Wikipedia|Kenichi Ogata (voice actor)}} << {{Wikipedia|Kenichi Ogata}}
{{Wikipedia|Kenshi Yonezu}}
{{Wikipedia|Kent Williams (voice actor)}} << {{Wikipedia|Kent Williams}}
{{Wikipedia|Kenta Kamakari}}
{{Wikipedia|Kenta Miyake}}
{{Wikipedia|Keratin}}
{{Wikipedia|Kinnikuman}}
{{Wikipedia|Kitty Pryde}}
{{Wikipedia|Kyoto Prefecture}}
{{Wikipedia|Kōji Yusa}} << {{W|voiceacting|Kōji Yusa}}
{{Wikipedia|Kōki Uchiyama}}
{{Wikipedia|Kōtarō Nishiyama}}
{{Wikipedia|Lampris guttatus}}
{{Wikipedia|Larry Brantley}}
{{Wikipedia|Leah Clark}}
{{Wikipedia|Levitation (paranormal)}}
{{Wikipedia|LiSA (Japanese musician, born 1987)}}
{{Wikipedia|List of experimental cat breeds#Mandalay}} << {{Wikipedia|List of minority cat breeds#Mandalay}}
{{Wikipedia|Luci Christian}}
{{Wikipedia|Luke Skywalker}}
{{Wikipedia|Maaya Uchida}}
{{Wikipedia|Mad Hatter (comics)}}
{{Wikipedia|Mainichi Broadcasting System}}
{{Wikipedia|Makoto Furukawa}}
{{Wikipedia|Manga}}
{{Wikipedia|Marina Inoue}}
{{Wikipedia|Mario (franchise)}}
{{Wikipedia|Marvel Comics}}
{{Wikipedia|Masaki Suda}}
{{Wikipedia|Matryoshka doll}}
{{Wikipedia|Matter}}
{{Wikipedia|Mendelian inheritance}}
{{Wikipedia|Meowth}} << {{Wikipedia|Inuko Inuyama}}
{{Wikipedia|Micah Solusod}}
{{Wikipedia|Michiko Neya}}
{{Wikipedia|Mie Prefecture}}
{{Wikipedia|Mie Sonozaki}} << {{Wikipedia| Mie Sonozaki}}
{{Wikipedia|Mike McFarland}}
{{Wikipedia|Minori Chihara}}
{{Wikipedia|Misato Fukuen}}
{{Wikipedia|Miwa (singer)}}
{{Wikipedia|Miyagi Prefecture}}
{{Wikipedia|Monica Rial}}
{{Wikipedia|Moon rabbit}} << {{Wikipedia|moon rabbit}}
{{Wikipedia|Movic}}
{{Wikipedia|My Little Pony: Friendship Is Magic}} << {{Wikipedia|My Little Pony: Friendship is Magic}}
{{Wikipedia|Mysophobia}}
{{Wikipedia|Nagasaki Prefecture}}
{{Wikipedia|Nagoya University of Arts}}
{{Wikipedia|Nara Prefecture}}
{{Wikipedia|Newton Pittman}}
{{Wikipedia|Night of the Living Dead (film series)}}
{{Wikipedia|Niigata Prefecture}}
{{Wikipedia|Nintendo 3DS}}
{{Wikipedia|Nippon TV}}
{{Wikipedia|Nitroglycerin}}
{{Wikipedia|Nobuhiko Okamoto}}
{{Wikipedia|Odor}} << {{Wikipedia|:Odor}}
{{Wikipedia|Okayama Prefecture}}
{{Wikipedia|Okinawa Prefecture}}
{{Wikipedia|Osaka Prefecture}}
{{Wikipedia|Otter Media}}
{{Wikipedia|Oumagadoki Zoo}} << {{Wikipedia|Oumadoki Zoo}}
{{Wikipedia|Oumagadoki Zoo}}
{{Wikipedia|Paralysis}}
{{Wikipedia|Parasite (comics)}}
{{Wikipedia|Patrick Seitz}}
{{Wikipedia|Phil Parsons (voice actor)}}
{{Wikipedia|Pixie-bob}}
{{Wikipedia|Plus ultra}}
{{Wikipedia|Porno Graffitti}} << {{Wikipedia|Porno Graffiti}}
{{Wikipedia|Porno Graffitti}}
{{Wikipedia|Precognition}}
{{Wikipedia|Ragdoll}}
{{Wikipedia|Raven (DC Comics)}}
{{Wikipedia|Remioromen}}
{{Wikipedia|Reservoir Dogs}}
{{Wikipedia|Ridley Scott}}
{{Wikipedia|Rob Liefeld}}
{{Wikipedia|Robert McCollum}}
{{Wikipedia|Ryō Hirohashi}}
{{Wikipedia|Ryōhei Kimura}}
{{Wikipedia|Sachi Kokuryu}}
{{Wikipedia|Saitama Prefecture}}
{{Wikipedia|San Diego Comic-Con}}
{{Wikipedia|Secondary education in Japan#Junior high school}}
{{Wikipedia|Shimane Prefecture}}
{{Wikipedia|Shinkansen}}
{{Wikipedia|Shinya Fukumatsu}}
{{Wikipedia|Shizuoka Prefecture}} << {{Wikipedia|Shizouka Prefecture}}
{{Wikipedia|Shizuoka Prefecture}}
{{Wikipedia|Shueisha}}
{{Wikipedia|Signalman (comics)}}
{{Wikipedia|Silver Age of Comic Books}}
{{Wikipedia|Sleepwalking}}
{{Wikipedia|Solubility}}
{{Wikipedia|Sonic weapon}}
{{Wikipedia|Sonny Strait}}
{{Wikipedia|Spats (footwear)}}
{{Wikipedia|Spawn (comics)}}
{{Wikipedia|Spider-Man}}
{{Wikipedia|Star Wars}}
{{Wikipedia|Star Wars: Episode I – The Phantom Menace}}
{{Wikipedia|Stephanie Young}} << {{Wikipedia|Stephanie Young Brehm}}
{{Wikipedia|Stephen King}}
{{Wikipedia|Structural formula}}
{{Wikipedia|Sturm und Drang}}
{{Wikipedia|Sulfur mustard}}
{{Wikipedia|Super Mario}}
{{Wikipedia|Super Mario Bros.}} << {{Wikipedia|Super Mario Bros}}
{{Wikipedia|Superman}}
{{Wikipedia|Suppressive fire}}
{{Wikipedia|TBS Television}}
{{Wikipedia|Tanegashima}}
{{Wikipedia|Tankōbon}}
{{Wikipedia|Tankōbon}} << {{Wikipedia|tankobon}}
{{Wikipedia|Tap (valve)}} << {{Wikipedia|Tap}}
{{Wikipedia|Tartarus}}
{{Wikipedia|Tasuku Hatanaka}}
{{Wikipedia|Tatooine}}
{{Wikipedia|Teen Titans (TV series)}}
{{Wikipedia|Teenage Mutant Ninja Turtles}}
{{Wikipedia|Tekkonkinkreet}}
{{Wikipedia|Telepathy}}
{{Wikipedia|Teleportation}}
{{Wikipedia|Tenosynovitis}} << {{Wikipedia|tenosynovitis}}
{{Wikipedia|Tesshō Genda}}
{{Wikipedia|Tetsu Inada}}
{{Wikipedia|Tetsuo: The Iron Man}}
{{Wikipedia|Textile}}
{{Wikipedia|Thailand}}
{{Wikipedia|The Addams Family}}
{{Wikipedia|The Batman}}
{{Wikipedia|The Three Musketeers}}
{{Wikipedia|Thor}}
{{Wikipedia|Tia Ballard}}
{{Wikipedia|Time Bokan 24}}
{{Wikipedia|Toad (comics)}}
{{Wikipedia|Todd McFarlane}}
{{Wikipedia|Tokyo}}
{{Wikipedia|Tomokazu Seki}}
{{Wikipedia|Tomy}}
{{Wikipedia|Tomy}} << {{Wikipedia|Tomy Arts}}
{{Wikipedia|Toonami}}
{{Wikipedia|Toshiki Masuda}}
{{Wikipedia|Tottori Prefecture}}
{{Wikipedia|Toyama Prefecture}}
{{Wikipedia|Trampoline}}
{{Wikipedia|Transgender}}
{{Wikipedia|Transitioning (transgender)}}
{{Wikipedia|Trickster (comics)}}
{{Wikipedia|Trident (DC Comics)}}
{{Wikipedia|Trina Nishimura}}
{{Wikipedia|Tsukuyomi-no-Mikoto}}
{{Wikipedia|Tungsten}} << {{Wikipedia|tungsten}}
{{Wikipedia|Tōru Nara}}
{{Wikipedia|Ultraman}}
{{Wikipedia|United States}} << {{Wikipedia|United States of America}}
{{Wikipedia|Uverworld}}
{{Wikipedia|Variable refrigerant flow}} << {{Wikipedia|VRV}}
{{Wikipedia|Viscosity}}
{{Wikipedia|Viz Media}}
{{Wikipedia|Wakayama Prefecture}}
{{Wikipedia|Warren Worthington III}}
{{Wikipedia|Wataru Hatano}}
{{Wikipedia|Weekly Shōnen Jump}}
{{Wikipedia|Wind}}
{{Wikipedia|Wolverine (character)}}
{{Wikipedia|X-Men}}
{{Wikipedia|Yamagata Prefecture}}
{{Wikipedia|Yamanashi Prefecture}}
{{Wikipedia|Yang Guifei}}
{{Wikipedia|Yarn}}
{{Wikipedia|Yasuhiro Takato}}
{{Wikipedia|Yomiuri Telecasting Corporation}}
{{Wikipedia|Yoshimasa Hosoya}} << {{Wikipedia|Yoshima Hosoya}}
{{Wikipedia|Yoshimasa Hosoya}}
{{Wikipedia|Yoshitsugu Matsuoka}} << {{Wikipedia| Yoshitsugu Matsuoka}}
{{Wikipedia|Yoyogi National Gymnasium}}
{{Wikipedia|Yuki Hayashi (composer)}}
{{Wikipedia|Yōsuke Kuroda}} << {{Wikipedia|Yosuke Kuroda}}
{{Wikipedia|Yōsuke Kuroda}} << {{Wikipedia|Yousuke Kuroda}}
{{Wikipedia|Yōsuke Kuroda}}
{{Wikipedia|Yūichirō Umehara}}
{{Wikipedia|Yūki Kaji}}
{{Wikipedia|ja:Amazarashi}}
{{Wikipedia|ja:Brian the Sun}} << {{Wikipedia|Brian the Sun}}
{{Wikipedia|ja:Lenny code fiction}}
{{Wikipedia|ja:Little Glee Monster}}
{{Wikipedia|ja:エンスカイ}} << {{Wikipedia|Ensky}}
{{Wikipedia|ja:古島清孝}} << {{Wikipedia|Kiyotaka Furushima}}
{{Wikipedia|ja:松田修平}} << {{Wikipedia|Shuhei Matsuda}}
