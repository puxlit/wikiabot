#!/usr/bin/env python3.7

import sys

from common import (SubstEffect, diff_pages, eprint, eprint_stats,
                    find_interwiki_links, load_candidate_pages,
                    load_remapped_links, openrt, openwt,
                    replace_interwiki_links, subset_main_pages_from_dump)

__all__ = ('main',)


##############################################################################

USAGE_MSG = ('usage: {0} pages_current.xml candidate_pages.txt '
             'remapped_links.txt simulated_edits.patch')
STATS_MSG = '''\
----------------
Out of   {0} candidate page{1},
examined {2} page{3} and
found    {4} IWL{5}
across   {6} page{7}.

Changed  {8} page{9}.

Found    {10} already optimal IWL{11},
changed  {12} IWL{13}, and
skipped  {14} unmapped IWL{15}.'''


def main() -> None:
    if len(sys.argv) != 5:
        eprint(USAGE_MSG.format(sys.argv[0]))
        sys.exit(1)

    pages_current_if = openrt(sys.argv[1])
    candidate_pages_if = openrt(sys.argv[2])
    remapped_links_if = openrt(sys.argv[3])
    simulated_edits_of = openwt(sys.argv[4])

    titles = load_candidate_pages(candidate_pages_if)
    pages = subset_main_pages_from_dump(pages_current_if, titles)
    mapping = load_remapped_links(remapped_links_if)

    num_matched_iwls = 0
    num_matched_pages = 0
    num_changed_pages = 0
    diffs = dict()
    num_noop_iwls = 0
    num_replaced_iwls = 0
    num_skipped_iwls = 0

    for (title, a_page) in sorted(pages.items()):
        matches = find_interwiki_links(a_page)
        if len(matches) == 0:
            continue
        num_matched_iwls += len(matches)
        num_matched_pages += 1

        eprint(f'  - for page [[{a_page.title}]]:')

        page_has_changed = False
        (b_page, substs) = replace_interwiki_links(a_page, matches, mapping)
        assert len(substs) == len(matches)
        for ((a_start, a_end, a_markup, a_link),
             (b_start, b_end, b_markup, b_link), effect) in substs:
            msg = f'      - @ ({b_start},{b_end}) << ({a_start},{a_end}): '
            if effect == SubstEffect.NOOP:
                num_noop_iwls += 1
                msg += '(noop)'
            elif effect == SubstEffect.REPLACED:
                page_has_changed = True
                num_replaced_iwls += 1
                msg += repr(b_markup)
            elif effect == SubstEffect.SKIPPED:
                num_skipped_iwls += 1
                msg += '(skipped)'
            msg += f' << {a_markup!r}'
            eprint(msg)

        if page_has_changed:
            num_changed_pages += 1
            diffs[title] = diff_pages(a_page, b_page)

    with simulated_edits_of:
        for (title, diff) in sorted(diffs.items()):
            simulated_edits_of.write(diff)

    num_candidate_pages = len(titles)
    num_examined_pages = len(pages)
    eprint_stats(STATS_MSG, num_candidate_pages, num_examined_pages,
                 num_matched_iwls, num_matched_pages, num_changed_pages,
                 num_noop_iwls, num_replaced_iwls, num_skipped_iwls)


##############################################################################

if __name__ == '__main__':
    main()
