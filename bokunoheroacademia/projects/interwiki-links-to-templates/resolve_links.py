#!/usr/bin/env python3.7

import sys
from typing import Dict, Set

from common import (LinkTarget, eprint, eprint_stats, load_interwiki_links,
                    openrt, openwt, partition, save_remapped_links)
from mwapi import LinkResolutionStats, mwapi, prepare_mwapi_logging

__all__ = ('main',)


##############################################################################

USAGE_MSG = 'usage: {0} interwiki_links.txt remapped_links.txt'
STATS_MSG = '''\
----------------
Resolved  {0} old IWL{1}
into      {2} new IWL{3}.

Preserved {4} already canonical IWL{5} and
updated   {6} outdated IWL{7}.

Dropped   {8} missing page IWL{9},
          {10} missing section IWL{11},
          {12} conficting sections IWL{13},
          {14} redirection loop IWL{15}, and
          {16} disambiguation page IWL{17}.'''


def main() -> None:
    if len(sys.argv) != 3:
        eprint(USAGE_MSG.format(sys.argv[0]))
        sys.exit(1)

    interwiki_links_if = openrt(sys.argv[1])
    remapped_links_of = openwt(sys.argv[2])

    targets = load_interwiki_links(interwiki_links_if)
    targets_by_wiki = partition(targets,
                                lambda lt: (lt.wiki_project, lt.wiki_name))

    inverse_mapping: Dict[LinkTarget, Set[LinkTarget]] = dict()
    stats = LinkResolutionStats(0, 0, 0, 0, 0, 0, 0)

    prepare_mwapi_logging(1)
    for ((wiki_project, wiki_name),
         wiki_targets) in sorted(targets_by_wiki.items()):
        eprint(f'  - for {wiki_name!r} {wiki_project.name.title()} with '
               f'{len(wiki_targets)} IWLs:')
        api = mwapi(wiki_project, wiki_name, 'resolve_links')
        api.canonicalize()
        graph = api.resolve_links(wiki_targets)
        for (to_target, from_targets) in graph.inverse_mapping.items():
            bucket = inverse_mapping.setdefault(to_target, set())
            assert all(from_target not in bucket
                       for from_target in from_targets)
            bucket.update(from_targets)
        stats = stats + graph.stats
        eprint('      - final resolutions are:')
        for chain in graph.chains:
            eprint(f'          - {chain.desc}')

    save_remapped_links(remapped_links_of, inverse_mapping)

    num_old_iwls = len(targets)
    num_new_iwls = len(inverse_mapping)
    eprint_stats(STATS_MSG, num_old_iwls, num_new_iwls, *stats)


##############################################################################

if __name__ == '__main__':
    main()
