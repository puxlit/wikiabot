from datetime import datetime, timezone
from difflib import unified_diff
from enum import IntEnum
from functools import wraps
import re
from sys import stderr, stdin, stdout
import time
from typing import (Any, Callable, Dict, IO, Iterable, List, Mapping, Match,
                    NamedTuple, Optional, Sequence, Set, Tuple, TypeVar, cast)
import xml.etree.ElementTree as ET

__all__ = (
    'iso8601_to_dt', 'rfc822_to_dt', 'dt_to_iso8601', 'dt_to_diff',

    'eprint', 'eprint_stats', 'openrt', 'openwt',

    'enforce_cooldown', 'partition',

    'Page',
    'is_valid_page_title',

    'WikiProject', 'LinkTarget', 'LinkStyle', 'Link',
    'is_valid_serialized_link',

    'LinkMatch', 'find_interwiki_links',
    'SubstEffect', 'LinkSubst', 'replace_interwiki_links',
    'diff_pages',

    'main_pages_from_dump', 'subset_main_pages_from_dump',
    'save_candidate_pages', 'load_candidate_pages',
    'save_interwiki_links', 'load_interwiki_links',
    'save_remapped_links', 'load_remapped_links',
)


##############################################################################

def iso8601_to_dt(iso8601: str) -> datetime:
    # We only care about a very specific format from ISO 8601.
    return datetime.strptime(iso8601, '%Y-%m-%dT%H:%M:%SZ') \
                   .replace(tzinfo=timezone.utc)


def rfc822_to_dt(rfc822: str) -> datetime:
    # We only care about a very specific format from RFC 822.
    return datetime.strptime(rfc822, '%a, %d %b %Y %H:%M:%S GMT') \
                   .replace(tzinfo=timezone.utc)


def dt_to_iso8601(dt: datetime) -> str:
    # We only care about a very specific format from ISO 8601.
    assert dt.tzinfo == timezone.utc
    return dt.strftime('%Y-%m-%dT%H:%M:%SZ')


def dt_to_diff(dt: datetime) -> str:
    return dt.strftime('%Y-%m-%d %H:%M:%S.%f000 %z')


##############################################################################

def eprint(*args: Any, **kwargs: Any) -> None:
    kwargs.update(file=stderr)
    return print(*args, **kwargs)


def eprint_stats(fmt: str, *counters: int) -> None:
    pretty_counters = ['{:,}'.format(c) for c in counters]
    max_width = max(len(pc) for pc in pretty_counters)
    justified_counters = [pc.rjust(max_width) for pc in pretty_counters]
    plurals = [('s' if c != 1 else '') for c in counters]
    fields = [f for fs in zip(justified_counters, plurals) for f in fs]
    eprint(fmt.format(*fields))


def openrt(file_path: str) -> IO:
    return stdin if (file_path == '-') else open(file_path, mode='rt')


def openwt(file_path: str) -> IO:
    return stdout if (file_path == '-') else open(file_path, mode='wt')


##############################################################################

TA = TypeVar('TA')
TB = TypeVar('TB')
TF = TypeVar('TF', bound=Callable[..., Any])


def enforce_cooldown(threshold: float, getter: Callable[[], float],
                     setter: Callable[[float], None]) -> Callable[[TF], TF]:
    def wrapper(f: TF) -> TF:
        @wraps(f)
        def impl(*args: Any, **kwargs: Any) -> Any:
            while True:
                secs_to_wait = threshold - (time.monotonic() - getter())
                if secs_to_wait <= 0:
                    break
                time.sleep(secs_to_wait)
            result = f(*args, **kwargs)
            setter(time.monotonic())
            return result
        # NB: Cast exists to appease mypy; see
        #     <https://github.com/python/mypy/issues/3157>.
        return cast(TF, impl)
    return wrapper


def partition(items: Set[TA], key: Callable[[TA], TB]) -> Dict[TB, Set[TA]]:
    buckets: Dict[TB, Set[TA]] = dict()
    for item in items:
        bucket: Set[TA] = buckets.setdefault(key(item), set())
        bucket.add(item)
    return buckets


##############################################################################

class Page(NamedTuple):
    pid: int
    rid: Optional[int]
    rts: Optional[datetime]
    title: str
    text: str

    @classmethod
    def from_strings(cls, pid: str, rid: str, rts: str,
                     title: str, text: str) -> 'Page':
        (typed_pid, typed_rid) = (int(pid), int(rid))
        assert ((typed_pid > 0) and (typed_rid > 0))
        typed_rts = iso8601_to_dt(rts)
        assert is_valid_page_title(title)
        return cls(typed_pid, typed_rid, typed_rts, title, text)

    @property
    def diff_filename(self) -> str:
        filename = f'{self.title} [page ID = {self.pid}'
        filename += f'; revision ID = {self.rid}]' \
                    if (self.rid is not None) else ']'
        return filename

    @property
    def diff_timestamp(self) -> str:
        return dt_to_diff(self.rts) if (self.rts is not None) else ''


# We're not interested in rigorously validating titles so much as
# ensuring we have a few delimiter character upon which we can rely.
PROBABLY_OKAY_TITLE_RE = re.compile(r'^[^\x00-\x1f#<>\[\]{\|}\x7f]+$')


def is_valid_page_title(title: str) -> bool:
    return bool(PROBABLY_OKAY_TITLE_RE.fullmatch(title))


##############################################################################

class WikiProject(IntEnum):
    WIKIA = 0
    WIKIPEDIA = 1


class LinkTarget(NamedTuple):
    wiki_project: WikiProject
    wiki_name: str
    page_title: str
    page_section: str

    @property
    def is_section_link(self) -> bool:
        return len(self.page_section) > 0

    @property
    def wiki_markup_fragment(self):
        if self.wiki_project == WikiProject.WIKIA:
            markup = f'W|{self.wiki_name}|'
        elif self.wiki_project == WikiProject.WIKIPEDIA:
            markup = 'Wikipedia|'
            if self.wiki_name != 'en':
                markup += self.wiki_name + ':'
        return markup

    @property
    def page_markup_fragment(self):
        markup = self.page_title
        if self.is_section_link:
            markup += '#' + self.page_section
        return markup

    @property
    def new_style_markup_fragment(self) -> str:
        return self.wiki_markup_fragment + self.page_markup_fragment

    @property
    def new_style_markup(self) -> str:
        return '{{' + self.new_style_markup_fragment + '}}'


class LinkStyle(IntEnum):
    NEW = 0
    OLD = 1


# Link trail pattern taken from
# <https://github.com/Wikia/app/blob/release-780.007/languages/messages/MessagesEn.php#L476-L480>.
LINK_TRAIL_RE = re.compile(r'^(?:[a-z]+)?$')


class Link(NamedTuple):
    r"""
    Parse interwiki link markup for representation as new-style markup.

    >>> Link.from_markup('[[wikipedia:Yūki Kaji|Yūki Kaji]]') \
    ...     .new_style_markup
    '{{Wikipedia|Yūki Kaji}}'
    >>> Link.from_markup('[[Wikipedia:Heterochromia iridum|heterochromia]]') \
    ...     .new_style_markup
    '{{Wikipedia|Heterochromia iridum|heterochromia}}'
    >>> Link.from_markup('[[wikia:c:avatar:Zuko|Zuko]]') \
    ...     .new_style_markup
    '{{W|avatar|Zuko}}'

    Note that canonicalization/normalization does not occur (here).

    >>> Link.from_markup('[[w:c:shingekinokyojin:Eren_Jaeger|Eren Jaeger]]') \
    ...     .new_style_markup
    '{{W|shingekinokyojin|Eren_Jaeger|Eren Jaeger}}'
    >>> # NB: Would be '{{W|attackontitan|Eren Jaeger}}'.

    However, some optimizations are supported.

    >>> Link.from_markup('[[Wikipedia:en:Amazarashi|amazarashi]]') \
    ...     .new_style_markup
    '{{Wikipedia|amazarashi}}'
    >>> Link.from_markup('[[Wikipedia:Black hole|black holes]]') \
    ...     .new_style_markup
    '{{Wikipedia|black hole}}s'
    >>> Link.from_markup('{{Wikipedia|black hole}}s') \
    ...     .new_style_markup
    '{{Wikipedia|black hole}}s'
    >>> Link.from_markup("[[Wikipedia:Black hole|black hole's]]") \
    ...     .new_style_markup
    "{{Wikipedia|Black hole|black hole's}}"
    >>> Link.from_markup("{{Wikipedia|black hole}}'s") \
    ...     .new_style_markup
    Traceback (most recent call last):
        ...
    AssertionError

    Links to Wikipedia articles in different languages are supported.

    >>> Link.from_markup('[[wikipedia:ja:Amazarashi|amazarashi]]') \
    ...     .new_style_markup
    '{{Wikipedia|ja:amazarashi}}'
    >>> Link.from_markup('{{Wikipedia|ja|Amazarashi|amazarashi}}') \
    ...     .new_style_markup
    '{{Wikipedia|ja:amazarashi}}'
    """

    style: LinkStyle
    target: LinkTarget
    label: Optional[str]

    @classmethod
    def from_markup(cls, markup: str) -> 'Link':
        assert len(markup) > 4
        head = markup[:2]
        assert ((head == '{{') or (head == '[['))
        # We'll assume the link trail can never contain '}}' or ']]'.
        if head == '{{':
            tail_start = markup.rindex('}}')
        elif head == '[[':
            tail_start = markup.rindex(']]')
        body = markup[2:tail_start]
        trail = markup[tail_start+2:]
        assert LINK_TRAIL_RE.fullmatch(trail)

        if head == '{{':
            style = LinkStyle.NEW

            bits = body.split('|', maxsplit=1)
            assert len(bits) == 2
            project_bit = bits[0].lower()
            assert ((project_bit == 'w') or (project_bit == 'wikipedia'))

            if project_bit == 'w':
                bits = bits[1].split('|', maxsplit=2)
                num_bits = len(bits)
                assert num_bits >= 2
                (project, name, title_and_section, label) = (
                    WikiProject.WIKIA, bits[0], bits[1],
                    bits[2] if (num_bits == 3) else None
                )
            elif project_bit == 'wikipedia':
                if ((len(bits[1]) >= 3) and (bits[1][2] == '|')):
                    # Sometimes people mess up the language code delimiter;
                    # account for that.
                    bits[1] = f'{bits[1][:2]}:{bits[1][3:]}'
                bits = bits[1].split('|', maxsplit=1)
                num_bits = len(bits)
                assert num_bits >= 1
                (project, name, title_and_section, label) = (
                    WikiProject.WIKIPEDIA, 'en', bits[0],
                    bits[1] if (num_bits == 2) else None
                )

        elif head == '[[':
            style = LinkStyle.OLD

            bits = body.split(':', maxsplit=1)
            assert len(bits) == 2
            project_bit = bits[0].lower()
            assert ((project_bit == 'w') or (project_bit == 'wikia') or
                    (project_bit == 'wikipedia'))

            if ((project_bit == 'w') or (project_bit == 'wikia')):
                assert bits[1][:2].lower() == 'c:'
                bits = bits[1][2:].split(':', maxsplit=1)
                if len(bits) == 1:
                    # Sometimes people mess up this markup; account for that.
                    bits = bits[0].split('|', maxsplit=1)
                assert len(bits) == 2
                (project, name) = (WikiProject.WIKIA, bits[0])
            elif (project_bit == 'wikipedia'):
                (project, name) = (WikiProject.WIKIPEDIA, 'en')

            bits = bits[1].split('|')
            num_bits = len(bits)
            assert (1 <= num_bits <= 2)
            (title_and_section, label) = (bits[0],
                                          bits[1] if (num_bits == 2) else None)

        bits = title_and_section.split('#')
        num_bits = len(bits)
        assert (1 <= num_bits <= 2)
        (title, section) = (bits[0], bits[1] if (num_bits == 2) else '')
        assert is_valid_page_title(title)

        # Roughly account for interlanguage links.
        if project == WikiProject.WIKIPEDIA:
            if ((len(title) >= 3) and (title[2] == ':')):
                language_code = title[0:2]
                if language_code.isalpha():
                    (name, title) = (language_code, title[3:])
            elif ((len(title) >= 5) and (title[:5] == 'test:')):
                # Special case the Wikipedia Test Wiki.
                (name, title) = ('test', title[5:])

        target = LinkTarget(project, name, title, section)

        # NB: We needn't support the pipe trick.
        assert ((label is None) or (len(label) > 0))
        if ((label == target.page_title) and ((project == WikiProject.WIKIA) or
                                              (len(name) == 2))):
            label = None

        if trail:
            if label:
                label += trail
            else:
                label = target.page_markup_fragment + trail

        return cls(style, target, label)

    @property
    def has_significant_label(self) -> bool:
        return self.label is not None

    @property
    def new_style_markup(self) -> str:
        markup = '{{'
        if not self.has_significant_label:
            markup += self.target.new_style_markup_fragment + '}}'
        else:
            markup += self.target.wiki_markup_fragment

            # NB: mypy can't infer the following from
            #     `self.has_significant_label`, hence the repetition.
            assert self.label is not None

            # Treat first letter as case-insensitive.
            page_markup_fragment = self.target.page_markup_fragment
            if page_markup_fragment[0].lower() == self.label[0].lower():
                page_markup_fragment = self.label[0] + page_markup_fragment[1:]

            possible_trail = self.label[len(page_markup_fragment):]
            markup += (f'{page_markup_fragment}}}}}{possible_trail}' if (
                self.label.startswith(page_markup_fragment) and
                LINK_TRAIL_RE.fullmatch(possible_trail)
            ) else f'{self.target.page_markup_fragment}|{self.label}}}}}')
        return markup


def is_valid_serialized_link(link: Link) -> bool:
    return ((link.style == LinkStyle.NEW) and not link.has_significant_label)


##############################################################################

class LinkMatch(NamedTuple):
    start: int
    end: int
    markup: str
    link: Link

    @classmethod
    def from_re_match(cls, match: Match) -> 'LinkMatch':
        (start, end, markup) = (match.start(), match.end(), match.group())
        link = Link.from_markup(markup)
        return cls(start, end, markup, link)


# Guess who's a fucking idiot for choosing regex over a proper wikitext parser?
# "There's no way people'd nest templates within IWLs," I thought. Then I go
# ahead shoot myself in the foot with
# [this](https://bokunoheroacademia.wikia.com/wiki/?diff=112291).
#
# >>> '{{Wikipedia|ja:Lenny code fiction|{{Ruby|Lenny|レニー}} ' \
# ... '{{Ruby|code|コード}} {{Ruby|fiction|フィクション}}}}'
#
# Anyway, the regex's been kludged to support _one_ level of template nesting
# for new-style links.
#
# Link trail pattern taken from
# <https://github.com/Wikia/app/blob/release-780.007/languages/messages/MessagesEn.php#L476-L480>.
IWL_MARKUP_RE = re.compile(r'(?i:'
                           r'{{W(?:ikipedia)?\|(?:[^{}](?:{{[^{}]*?}})?)*?}}'
                           r'|'
                           r'\[\[w(?::c|iki(?:a|pedia)):.*?]]'
                           r')(?:[a-z]+)?')


def find_interwiki_links(page: Page) -> Sequence[LinkMatch]:
    return [LinkMatch.from_re_match(match)
            for match in
            IWL_MARKUP_RE.finditer(page.text)]


class SubstEffect(IntEnum):
    NOOP = 0
    REPLACED = 1
    SKIPPED = 2


class LinkSubst(NamedTuple):
    a: LinkMatch
    b: LinkMatch
    effect: SubstEffect


def replace_interwiki_links(page: Page, matches: Sequence[LinkMatch],
                            mapping: Mapping[LinkTarget, LinkTarget]
                            ) -> Tuple[Page, Sequence[LinkSubst]]:
    substs: List[LinkSubst] = []

    a_cursor = 0
    (a_text, b_text) = (page.text, '')
    for a_match in matches:
        (a_start, a_end, a_markup, a_link) = a_match
        assert a_text[a_start:a_end] == a_markup

        if a_link.target in mapping:
            b_linktarget = mapping[a_link.target]
            a_link_label = a_link.label or a_link.target.page_markup_fragment
            b_link_label = (None if (
                a_link_label == b_linktarget.page_markup_fragment
            ) else a_link_label)
            b_link = Link(LinkStyle.NEW, b_linktarget, b_link_label)
            b_markup = b_link.new_style_markup
            effect = (SubstEffect.NOOP
                      if (b_markup == a_markup) else
                      SubstEffect.REPLACED)
        else:
            (b_markup, b_link) = (a_markup, a_link)
            effect = SubstEffect.SKIPPED

        b_text += a_text[a_cursor:a_start]
        b_start = len(b_text)
        b_text += b_markup
        b_end = len(b_text)
        a_cursor = a_end

        b_match = LinkMatch(b_start, b_end, b_markup, b_link)
        substs.append(LinkSubst(a_match, b_match, effect))
    b_text += a_text[a_cursor:]

    return (Page(page.pid, None, None, page.title, b_text), substs)


def diff_pages(a: Page, b: Page) -> str:
    a_lines = a.text.splitlines(keepends=True)
    b_lines = b.text.splitlines(keepends=True)
    diff_lines = list(unified_diff(
        a_lines, b_lines,
        fromfile=a.diff_filename, tofile=b.diff_filename,
        fromfiledate=a.diff_timestamp, tofiledate=b.diff_timestamp
    ))

    # Work around <https://bugs.python.org/issue11632> naively.
    assert all(line.endswith('\n') for line in diff_lines[:-2])
    diff = ''.join(diff_lines[:-2])
    for line in diff_lines[-2:]:
        diff += line
        if not line.endswith('\n'):
            diff += '\n\\ No newline at end of file\n'

    return diff


##############################################################################

MW_XMLNS_URI = 'http://www.mediawiki.org/xml/export-0.10/'
MW_MEDIAWIKI_TAG = f'{{{MW_XMLNS_URI}}}mediawiki'
MW_PAGE_TAG = f'{{{MW_XMLNS_URI}}}page'
MW_NS_TAG = f'{{{MW_XMLNS_URI}}}ns'
MW_ID_TAG = f'{{{MW_XMLNS_URI}}}id'
MW_REVISION_TAG = f'{{{MW_XMLNS_URI}}}revision'
MW_REVISION_ID_XPATH = f'{MW_REVISION_TAG}/{MW_ID_TAG}'
MW_TITLE_TAG = f'{{{MW_XMLNS_URI}}}title'
MW_TIMESTAMP_TAG = f'{{{MW_XMLNS_URI}}}timestamp'
MW_REVISION_TIMESTAMP_XPATH = f'{MW_REVISION_TAG}/{MW_TIMESTAMP_TAG}'
MW_TEXT_TAG = f'{{{MW_XMLNS_URI}}}text'
MW_REVISION_TEXT_XPATH = f'{MW_REVISION_TAG}/{MW_TEXT_TAG}'


def main_pages_from_dump(in_file: IO[str]) -> Iterable[Page]:
    context = iter(ET.iterparse(in_file, events=('start', 'end')))

    (event, root) = next(context)
    assert ((event == 'start') and (root.tag == MW_MEDIAWIKI_TAG))

    pids: Set[str] = set()
    rids: Set[str] = set()
    titles: Set[str] = set()

    for (event, elem) in context:
        if not ((event == 'end') and (elem.tag == MW_PAGE_TAG)):
            continue

        ns = elem.find(MW_NS_TAG).text
        if ns == '0':
            pid = elem.find(MW_ID_TAG).text
            rid = elem.find(MW_REVISION_ID_XPATH).text
            title = elem.find(MW_TITLE_TAG).text
            assert ((pid not in pids) and
                    (rid not in rids) and
                    (title not in titles))
            pids.add(pid)
            rids.add(rid)
            titles.add(title)

            rts = elem.find(MW_REVISION_TIMESTAMP_XPATH).text
            text = elem.find(MW_REVISION_TEXT_XPATH).text
            yield Page.from_strings(pid, rid, rts, title, text)

        elem.clear()
        root.clear()


def subset_main_pages_from_dump(in_file: IO[str],
                                titles: Set[str]) -> Mapping[str, Page]:
    pages: Dict[str, Page] = dict()
    for page in main_pages_from_dump(in_file):
        if page.title in titles:
            pages[page.title] = page
    return pages


def save_candidate_pages(out_file: IO[str], titles: Set[str]) -> None:
    with out_file:
        for title in sorted(titles):
            out_file.write(title + '\n')


def load_candidate_pages(in_file: IO[str]) -> Set[str]:
    titles: Set[str] = set()
    with in_file:
        for line in in_file:
            title = line.strip()
            assert (is_valid_page_title(title) and (title not in titles))
            titles.add(title)
    return titles


def save_interwiki_links(out_file: IO[str], targets: Set[LinkTarget]) -> None:
    with out_file:
        for target in sorted(targets):
            out_file.write(target.new_style_markup + '\n')


def load_interwiki_links(in_file: IO[str]) -> Set[LinkTarget]:
    targets: Set[LinkTarget] = set()
    with in_file:
        for line in in_file:
            link = Link.from_markup(line.strip())
            assert (is_valid_serialized_link(link) and
                    (link.target not in targets))
            targets.add(link.target)
    return targets


def save_remapped_links(out_file: IO[str],
                        inverse_mapping: Mapping[LinkTarget,
                                                 Set[LinkTarget]]) -> None:
    with out_file:
        for (to_target, from_targets) in sorted(inverse_mapping.items()):
            for from_target in sorted(from_targets):
                if from_target == to_target:
                    line = to_target.new_style_markup + '\n'
                else:
                    line = (f'{to_target.new_style_markup} << '
                            f'{from_target.new_style_markup}\n')
                out_file.write(line)


def load_remapped_links(in_file: IO[str]) -> Mapping[LinkTarget, LinkTarget]:
    mapping: Dict[LinkTarget, LinkTarget] = dict()
    with in_file:
        for line in in_file:
            links = [Link.from_markup(link_markup.strip())
                     for link_markup in line.split('<<')]
            num_links = len(links)
            assert ((1 <= num_links <= 2) and
                    all(is_valid_serialized_link(link) for link in links))
            to_target = links[0].target
            from_target = links[1].target if (num_links == 2) else to_target
            assert from_target not in mapping
            mapping[from_target] = to_target
    return mapping
