{{W|Kekkai-Sensen|Klaus Von Reinherz}}
{{W|akamegakill|Incursio}}
{{W|akira|Akira (Manga)}}
{{W|animevoiceover|Aaron Dismuke}}
{{W|animevoiceover|Christopher R. Sabat}}
{{W|animevoiceover|Clifford Chapin}}
{{W|animevoiceover|Colleen Clinkenbeard}}
{{W|animevoiceover|David Matranga}}
{{W|animevoiceover|David Wald}}
{{W|animevoiceover|Felecia Angelle}}
{{W|animevoiceover|Jarrod Greene}}
{{W|animevoiceover|Josh Grelle}}
{{W|animevoiceover|Justin Briner}}
{{W|animevoiceover|Kate Oxley}}
{{W|animevoiceover|Lara Woodhull}}
{{W|animevoiceover|Luci Christian}}
{{W|animevoiceover|Morgan Berry}}
{{W|attackontitan|Eren Jaeger}}
{{W|avatar|Zuko}}
{{W|avp|Caterpillar P-5000 Work Loader}}
{{W|bleach|Ichigo Kurosaki}}
{{W|community|Community Central}}
{{W|dc|Dick Grayson}}
{{W|dc|Nathaniel Heywood (New Earth)}}
{{W|dc|Superman}}
{{W|dragonball|Bubbles}}
{{W|dragonball|Final Flash}}
{{W|dragonball|King Kai}}
{{W|fairytail|Gajeel Redfox}}
{{W|fairytail|Lyon Vastia}}
{{W|hunterxhunter|Gon Freecss}}
{{W|jojo|Jotaro Kujo}}
{{W|jojo|Stand Cry}}
{{W|jojo|Star Platinum}}
{{W|mariowiki|Piranha Plant}}
{{W|mariowiki|Thwomp}}
{{W|mariowiki|goomba}}
{{W|mariowiki|koopa (species)}}
{{W|marvel|Piotr Rasputin (Earth-616)}}
{{W|marvel|Spider-Man}}
{{W|marvel|Steven Rogers (Earth-616)}}
{{W|nanatsu-no-taizai|Meliodas}}
{{W|naruto|Inari}}
{{W|naruto|Naruto}}
{{W|onepiece|One Piece (Manga)}}
{{W|oumagadokizoo|Chapter 0}}
{{W|oumagadokizoo|Hana}}
{{W|oumagadokizoo|Hana Aoi}}
{{W|oumagadokizoo|Isana}}
{{W|oumagadokizoo|Kōhei Horikoshi}}
{{W|oumagadokizoo|Sakamata}}
{{W|oumagadokizoo|Shiina}}
{{W|oumagadokizoo|Shikuma}}
{{W|oumagadokizoo|Shishido}}
{{W|oumagadokizoo|Takahiro}}
{{W|oumagadokizoo|Toytoy}}
{{W|oumagadokizoo|Uwabami}}
{{W|oumagadokizoo|Ōmagadoki Dōbutsuen}}
{{W|powerrangers|Anubis Cruger}}
{{W|powerrangers|Doggie Kruger}}
{{W|powerrangers|Power Rangers S.P.D.}}
{{W|powerrangers|Tokusou Sentai Dekaranger}}
{{W|senseinobulge|Kōhei Horikoshi}}
{{W|senseinobulge|Sensei no Bulge}}
{{W|senseinobulge|Uchū Shōnen Bulge}}
{{W|starwars|''Star Wars''}}
{{W|starwars|Alderaan}}
{{W|starwars|Hoth}}
{{W|starwars|Kashyyyk}}
{{W|starwars|Luke Skywalker}}
{{W|starwars|Star Wars: Episode IV A New Hope}}
{{W|starwars|Tatooine}}
{{W|starwars|Wookiee}}
{{W|starwars|Yoda}}
{{W|voiceacting|Alex Organ}}
{{W|voiceacting|Charlie Campbell}}
{{W|voiceacting|Christopher Wehkamp}}
{{W|voiceacting|Jessica Cavanagh}}
{{W|voiceacting|Justin Briner}}
{{W|voiceacting|Kyle Phillips}}
{{W|voiceacting|Kōji Yusa}}
{{W|worldtrigger|Chika Amatori}}
{{W|worldtrigger|Ōji Unit}}
{{W|x-men|Cyclops (Scott Summers)}}
{{W|x-men|Wolverine (James "Logan" Howlett)}}
{{W|zelda|Deku}}
{{Wikipedia| Adult Swim}}
{{Wikipedia| Jerry Jewell}}
{{Wikipedia| Mie Sonozaki}}
{{Wikipedia| Yoshitsugu Matsuoka}}
{{Wikipedia|:Odor}}
{{Wikipedia|Acid}}
{{Wikipedia|Adhesive}}
{{Wikipedia|Adsorption}}
{{Wikipedia|Adult Swim}}
{{Wikipedia|Aichi Prefecture}}
{{Wikipedia|Akame ga Kill!}}
{{Wikipedia|Akeno Watanabe}}
{{Wikipedia|Akihabara}}
{{Wikipedia|Akio Ōtsuka}}
{{Wikipedia|Akita Prefecture}}
{{Wikipedia|Alexis Tipton}}
{{Wikipedia|Alien (film)}}
{{Wikipedia|Alien (franchise)}}
{{Wikipedia|Ami Koshimizu}}
{{Wikipedia|Anastasia Muñoz}}
{{Wikipedia|Anime Expo}}
{{Wikipedia|Anthony Bowling}}
{{Wikipedia|Anti-gravity}}
{{Wikipedia|Aoi Yūki}}
{{Wikipedia|Aomori Prefecture}}
{{Wikipedia|Arcade Game}}
{{Wikipedia|Austin Tindle}}
{{Wikipedia|Avengers}}
{{Wikipedia|Ayane Sakura}}
{{Wikipedia|Bandai Namco}}
{{Wikipedia|Bandai Namco Entertainment}}
{{Wikipedia|Banpresto}}
{{Wikipedia|Black hole}}
{{Wikipedia|Bleach (TV series)}}
{{Wikipedia|Blood}}
{{Wikipedia|Blood Blockade Battlefront}}
{{Wikipedia|Blood type}}
{{Wikipedia|Booster Gold}}
{{Wikipedia|Boys on the Run}}
{{Wikipedia|Brainwashing}}
{{Wikipedia|Brandon Potter}}
{{Wikipedia|Brian the Sun}}
{{Wikipedia|Brina Palencia}}
{{Wikipedia|Bronze Age of Comic Books}}
{{Wikipedia|Bryn Apprill}}
{{Wikipedia|Bubaigawara Station}}
{{Wikipedia|Bubble (physics)}}
{{Wikipedia|Bōjutsu}}
{{Wikipedia|Caitlin Glass}}
{{Wikipedia|Captain America}}
{{Wikipedia|Cassandra Lang}}
{{Wikipedia|Cassandra Lee Morris}}
{{Wikipedia|Cement}}
{{Wikipedia|Cenobite (Hellraiser)}}
{{Wikipedia|Cheongsam}}
{{Wikipedia|Cherami Leigh}}
{{Wikipedia|Chiba Prefecture}}
{{Wikipedia|China}}
{{Wikipedia|Chris Rager}}
{{Wikipedia|Christopher Bevins}}
{{Wikipedia|Christopher Sabat}}
{{Wikipedia|Chronostasis}}
{{Wikipedia|Chuck Huber}}
{{Wikipedia|Cis AB}}
{{Wikipedia|Clifford Chaplin}}
{{Wikipedia|Clint Eastwood}}
{{Wikipedia|Clone Saga}}
{{Wikipedia|Cloning}}
{{Wikipedia|Close Quarters Combat}}
{{Wikipedia|Colleen Clinkenbeard}}
{{Wikipedia|Compression (physics)}}
{{Wikipedia|Confession}}
{{Wikipedia|Coordinate system}}
{{Wikipedia|Copyrights}}
{{Wikipedia|Corrosion}}
{{Wikipedia|Cousin Itt}}
{{Wikipedia|Cris George}}
{{Wikipedia|Cyclops (Marvel Comics)}}
{{Wikipedia|Daiki Yamashita}}
{{Wikipedia|Darth Vader}}
{{Wikipedia|David Matranga}}
{{Wikipedia|David Wald}}
{{Wikipedia|Deadpool}}
{{Wikipedia|Decomposition}}
{{Wikipedia|Doping in sport}}
{{Wikipedia|Dragon Ball}}
{{Wikipedia|Dry eye syndrome}}
{{Wikipedia|Dust}}
{{Wikipedia|Earthflow}}
{{Wikipedia|Ectoplasm (paranormal)}}
{{Wikipedia|Ehime Prefecture}}
{{Wikipedia|Elasticity (physics)}}
{{Wikipedia|Electricity}}
{{Wikipedia|Emily Neves}}
{{Wikipedia|Ensky}}
{{Wikipedia|Entomophobia}}
{{Wikipedia|Eri Kitamura}}
{{Wikipedia|Eric Vale}}
{{Wikipedia|Etsuko Kozakura}}
{{Wikipedia|Explosion}}
{{Wikipedia|Fair use}}
{{Wikipedia|Fantastic Four}}
{{Wikipedia|Fat}}
{{Wikipedia|Felicia Angelle}}
{{Wikipedia|Firestorm (comics)}}
{{Wikipedia|Flight}}
{{Wikipedia|Ford Torino}}
{{Wikipedia|Four past Midnight}}
{{Wikipedia|France}}
{{Wikipedia|Francis Xavier}}
{{Wikipedia|Frog}}
{{Wikipedia|Fukuoka Prefecture}}
{{Wikipedia|Fukushima Prefecture}}
{{Wikipedia|Garganta}}
{{Wikipedia|George A. Romero}}
{{Wikipedia|Ghost}}
{{Wikipedia|Giants (Greek mythology)#The Gigantomachy}}
{{Wikipedia|Giganta}}
{{Wikipedia|Golem}}
{{Wikipedia|Gran Torino}}
{{Wikipedia|Greg Ayres}}
{{Wikipedia|Gunma Prefecture}}
{{Wikipedia|Hand_Fan}}
{{Wikipedia|Hardness}}
{{Wikipedia|Hellraiser (franchise)}}
{{Wikipedia|Heterochromia iridum}}
{{Wikipedia|Hikaru Midorikawa}}
{{Wikipedia|Hime cut}}
{{Wikipedia|Hiro Shimono}}
{{Wikipedia|Hiroshima Prefecture}}
{{Wikipedia|Hiroyuki Yoshino}}
{{Wikipedia|Hiroyuki Yoshino (voice actor)}}
{{Wikipedia|Hokkaido}}
{{Wikipedia|Homeroom#Japan}}
{{Wikipedia|Hulk (comics)}}
{{Wikipedia|Hulu}}
{{Wikipedia|Human voice}}
{{Wikipedia|Hunter × Hunter}}
{{Wikipedia|Ian Sinclair (voice actor)}}
{{Wikipedia|Indigenous peoples of the Americas}}
{{Wikipedia|Inuko Inuyama}}
{{Wikipedia|Invincible (comics)#Enemies}}
{{Wikipedia|Iron Man}}
{{Wikipedia|Irresistible force paradox}}
{{Wikipedia|Iwate Prefecture}}
{{Wikipedia|J Michael Tatum}}
{{Wikipedia|J. Michael Tatum}}
{{Wikipedia|Jad Saxton}}
{{Wikipedia|Jamie Madrox}}
{{Wikipedia|Jamie Marchi}}
{{Wikipedia|Japanese armor}}
{{Wikipedia|Japanese school uniform}}
{{Wikipedia|Japanese school uniform#Gakuran}}
{{Wikipedia|Jason Douglas}}
{{Wikipedia|Jason Liebrecht}}
{{Wikipedia|Jeremy Inman}}
{{Wikipedia|Jiaozi#Gyōza}}
{{Wikipedia|John Burgmeier}}
{{Wikipedia|John Swasey}}
{{Wikipedia|Jojo's Bizarre Adventure}}
{{Wikipedia|Josh Grelle}}
{{Wikipedia|Juli Erickson}}
{{Wikipedia|Junichi Suwabe}}
{{Wikipedia|Justin Cook}}
{{Wikipedia|Kagawa Prefecture}}
{{Wikipedia|Kagoshima Prefecture}}
{{Wikipedia|Kaito Ishikawa}}
{{Wikipedia|Kanagawa Prefecture}}
{{Wikipedia|Kanji}}
{{Wikipedia|Kaori Nazuka}}
{{Wikipedia|Katakana}}
{{Wikipedia|Kei Shindō}}
{{Wikipedia|Kenichi Ogata}}
{{Wikipedia|Kenshi Yonezu}}
{{Wikipedia|Kent Williams}}
{{Wikipedia|Kenta Kamakari}}
{{Wikipedia|Kenta Miyake}}
{{Wikipedia|Keratin}}
{{Wikipedia|Kinnikuman}}
{{Wikipedia|Kitty Pryde}}
{{Wikipedia|Kiyotaka Furushima}}
{{Wikipedia|Kyoto Prefecture}}
{{Wikipedia|Kōki Uchiyama}}
{{Wikipedia|Kōtarō Nishiyama}}
{{Wikipedia|Lampris guttatus}}
{{Wikipedia|Larry Brantley}}
{{Wikipedia|Leah Clark}}
{{Wikipedia|Levitation (paranormal)}}
{{Wikipedia|LiSA (Japanese musician, born 1987)}}
{{Wikipedia|List of minority cat breeds#Mandalay}}
{{Wikipedia|Luci Christian}}
{{Wikipedia|Luke Skywalker}}
{{Wikipedia|Maaya Uchida}}
{{Wikipedia|Mad Hatter (comics)}}
{{Wikipedia|Mainichi Broadcasting System}}
{{Wikipedia|Makoto Furukawa}}
{{Wikipedia|Manga}}
{{Wikipedia|Marina Inoue}}
{{Wikipedia|Mario (franchise)}}
{{Wikipedia|Marvel Comics}}
{{Wikipedia|Masaki Suda}}
{{Wikipedia|Matryoshka doll}}
{{Wikipedia|Matter}}
{{Wikipedia|Mendelian inheritance}}
{{Wikipedia|Micah Solusod}}
{{Wikipedia|Michiko Neya}}
{{Wikipedia|Mie Prefecture}}
{{Wikipedia|Mike McFarland}}
{{Wikipedia|Minori Chihara}}
{{Wikipedia|Misato Fukuen}}
{{Wikipedia|Miwa (singer)}}
{{Wikipedia|Miyagi Prefecture}}
{{Wikipedia|Monica Rial}}
{{Wikipedia|Movic}}
{{Wikipedia|My Little Pony: Friendship is Magic}}
{{Wikipedia|Mysophobia}}
{{Wikipedia|Nagasaki Prefecture}}
{{Wikipedia|Nagoya University of Arts}}
{{Wikipedia|Nara Prefecture}}
{{Wikipedia|Newton Pittman}}
{{Wikipedia|Night of the Living Dead (film series)}}
{{Wikipedia|Niigata Prefecture}}
{{Wikipedia|Nintendo 3DS}}
{{Wikipedia|Nippon TV}}
{{Wikipedia|Nitroglycerin}}
{{Wikipedia|Nobuhiko Okamoto}}
{{Wikipedia|Okayama Prefecture}}
{{Wikipedia|Okinawa Prefecture}}
{{Wikipedia|Osaka Prefecture}}
{{Wikipedia|Otter Media}}
{{Wikipedia|Oumadoki Zoo}}
{{Wikipedia|Oumagadoki Zoo}}
{{Wikipedia|Paralysis}}
{{Wikipedia|Parasite (comics)}}
{{Wikipedia|Patrick Seitz}}
{{Wikipedia|Phil Parsons (voice actor)}}
{{Wikipedia|Pixie-bob}}
{{Wikipedia|Plus ultra}}
{{Wikipedia|Porno Graffiti}}
{{Wikipedia|Porno Graffitti}}
{{Wikipedia|Precognition}}
{{Wikipedia|Qipao}}
{{Wikipedia|Ragdoll}}
{{Wikipedia|Raven (DC Comics)}}
{{Wikipedia|Remioromen}}
{{Wikipedia|Reservoir Dogs}}
{{Wikipedia|Ridley Scott}}
{{Wikipedia|Rob Liefeld}}
{{Wikipedia|Robert McCollum}}
{{Wikipedia|Ryō Hirohashi}}
{{Wikipedia|Ryōhei Kimura}}
{{Wikipedia|Sachi Kokuryu}}
{{Wikipedia|Saitama Prefecture}}
{{Wikipedia|San Diego Comic-Con}}
{{Wikipedia|Secondary education in Japan#Junior high school}}
{{Wikipedia|Shimane Prefecture}}
{{Wikipedia|Shinkansen}}
{{Wikipedia|Shinya Fukumatsu}}
{{Wikipedia|Shizouka Prefecture}}
{{Wikipedia|Shizuoka Prefecture}}
{{Wikipedia|Shueisha}}
{{Wikipedia|Shuhei Matsuda}}
{{Wikipedia|Signalman (comics)}}
{{Wikipedia|Silver Age of Comic Books}}
{{Wikipedia|Sleepwalking}}
{{Wikipedia|Solubility}}
{{Wikipedia|Sonic weapon}}
{{Wikipedia|Sonny Strait}}
{{Wikipedia|Spats (footwear)}}
{{Wikipedia|Spawn (comics)}}
{{Wikipedia|Spider-Man}}
{{Wikipedia|Star Wars}}
{{Wikipedia|Star Wars: Episode I – The Phantom Menace}}
{{Wikipedia|Stephanie Young Brehm}}
{{Wikipedia|Stephen King}}
{{Wikipedia|Structural formula}}
{{Wikipedia|Sturm und Drang}}
{{Wikipedia|Sulfur mustard}}
{{Wikipedia|Super Mario}}
{{Wikipedia|Super Mario Bros}}
{{Wikipedia|Superman}}
{{Wikipedia|Suppressive fire}}
{{Wikipedia|TBS Television}}
{{Wikipedia|Tanegashima}}
{{Wikipedia|Tankōbon}}
{{Wikipedia|Tap}}
{{Wikipedia|Tartarus}}
{{Wikipedia|Tasuku Hatanaka}}
{{Wikipedia|Tatooine}}
{{Wikipedia|Teen Titans (TV series)}}
{{Wikipedia|Teenage Mutant Ninja Turtles}}
{{Wikipedia|Tekkonkinkreet}}
{{Wikipedia|Telepathy}}
{{Wikipedia|Teleportation}}
{{Wikipedia|Tesshō Genda}}
{{Wikipedia|Tetsu Inada}}
{{Wikipedia|Tetsuo: The Iron Man}}
{{Wikipedia|Textile}}
{{Wikipedia|Thailand}}
{{Wikipedia|The Addams Family}}
{{Wikipedia|The Batman}}
{{Wikipedia|The Flash}}
{{Wikipedia|The Three Musketeers}}
{{Wikipedia|Thor}}
{{Wikipedia|Tia Ballard}}
{{Wikipedia|Time Bokan 24}}
{{Wikipedia|Toad (comics)}}
{{Wikipedia|Todd McFarlane}}
{{Wikipedia|Tokyo}}
{{Wikipedia|Tomokazu Seki}}
{{Wikipedia|Tomy}}
{{Wikipedia|Tomy Arts}}
{{Wikipedia|Toonami}}
{{Wikipedia|Toshiki Masuda}}
{{Wikipedia|Tottori Prefecture}}
{{Wikipedia|Toyama Prefecture}}
{{Wikipedia|Trampoline}}
{{Wikipedia|Transgender}}
{{Wikipedia|Transitioning (transgender)}}
{{Wikipedia|Trickster (comics)}}
{{Wikipedia|Trident (DC Comics)}}
{{Wikipedia|Trina Nishimura}}
{{Wikipedia|Tsukuyomi-no-Mikoto}}
{{Wikipedia|Tōru Nara}}
{{Wikipedia|Ultraman}}
{{Wikipedia|United States of America}}
{{Wikipedia|Uverworld}}
{{Wikipedia|VRV}}
{{Wikipedia|Viscosity}}
{{Wikipedia|Viz Media}}
{{Wikipedia|Wakayama Prefecture}}
{{Wikipedia|Warren Worthington III}}
{{Wikipedia|Wataru Hatano}}
{{Wikipedia|Weekly Shōnen Jump}}
{{Wikipedia|Wind}}
{{Wikipedia|Wolverine (character)}}
{{Wikipedia|X-Men}}
{{Wikipedia|Yamagata Prefecture}}
{{Wikipedia|Yamanashi Prefecture}}
{{Wikipedia|Yang Guifei}}
{{Wikipedia|Yarn}}
{{Wikipedia|Yasuhiro Takato}}
{{Wikipedia|Yomiuri Telecasting Corporation}}
{{Wikipedia|Yoshima Hosoya}}
{{Wikipedia|Yoshimasa Hosoya}}
{{Wikipedia|Yosuke Kuroda}}
{{Wikipedia|Yousuke Kuroda}}
{{Wikipedia|Yoyogi National Gymnasium}}
{{Wikipedia|Yuki Hayashi (composer)}}
{{Wikipedia|Yōsuke Kuroda}}
{{Wikipedia|Yūichirō Umehara}}
{{Wikipedia|Yūki Kaji}}
{{Wikipedia|grappling hook}}
{{Wikipedia|hakama}}
{{Wikipedia|heart}}
{{Wikipedia|moon rabbit}}
{{Wikipedia|tankobon}}
{{Wikipedia|tenosynovitis}}
{{Wikipedia|tungsten}}
{{Wikipedia|ja:Amazarashi}}
{{Wikipedia|ja:Lenny code fiction}}
{{Wikipedia|ja:Little Glee Monster}}
