from contextlib import contextmanager
from datetime import datetime
from enum import IntEnum
from functools import partial
from hashlib import md5
from itertools import count
import logging
import re
from typing import (Callable, ContextManager, Dict, Generator, Mapping,
                    NamedTuple, Optional, Pattern, Sequence, Set, Tuple)

from requests import PreparedRequest, Request, Response, Session

from common import (LinkTarget, Page, WikiProject, dt_to_iso8601,
                    enforce_cooldown, is_valid_page_title, iso8601_to_dt,
                    partition, rfc822_to_dt)

__all__ = (
    'prepare_mwapi_logging',

    'EditContext', 'LinkResolutionStats', 'LinkResolutionGraph',
    'MediaWikiAPI', 'WikiaAPI', 'WikipediaAPI',
    'mwapi',
)


##############################################################################

logger = logging.getLogger(__name__)


def prepare_mwapi_logging(indent_level: int) -> None:
    formatter = logging.Formatter(
        ('    ' * indent_level) +
        '  - [{response_timestamp:%Y-%m-%dT%H:%M:%SZ}]: {message:s}',
        datefmt='', style='{'
    )
    handler = logging.StreamHandler()
    handler.setLevel(logging.INFO)
    handler.setFormatter(formatter)
    logger.setLevel(logging.INFO)
    logger.addHandler(handler)


##############################################################################

'''
Here's a summary of relevant API similarities and differences between Wikia and
Wikipedia, determined empirically and through pouring over documentation, valid
as at time of writing.

MediaWiki version (with `action=query&meta=siteinfo&siprop=general`):
  - Wikia     : 1.19.24
  - Wikipedia : 1.32.0-wmf.12

Practically every query will have `format=json&action=…`. For bluntly asserting
success, ensure `errors`/`error`/`warnings` aren't in the result. For bluntly
asserting we don't have any continuations to follow, ensure neither `continue`
nor `query-continue` are in the result.

To get around potential URL size limits when making GET requests (as described
in <https://www.mediawiki.org/wiki/API:FAQ#do_really_long_API_urls_not_work?>),
we just POST everything. To ensure our POSTs are efficiently encoded (as
described in <https://www.mediawiki.org/wiki/API:Edit#Large_texts>), we dumbly
prepare requests in both urlencoded and multipart variants, and send whichever
would be smaller.

For timestamping results:
  - from Wikia, with `action=query` (and at the expense of a bloated response),
    `meta=siteinfo&siprop=general` → `query.general.time`
  - from Wikia, with any action, inspect the `date` header, which should at
    most be a couple of seconds ahead of what the backend would have
    timestamped the results as
  - from Wikipedia, with any action, `curtimestamp=` → `curtimestamp`

For determining whether pages are disambiguations during `action=query`:
  - from Wikia:
      - `list=allcategories&acprefix=Disambig&aclimit=10` first to enumerate
        potential categories
          - usually it's one of `Category:Disambiguation`,
            `Category:Disambiguation pages`, or `Category:Disambiguations`
          - sometimes there won't be a category
          - it's unlikely to be hundreds of different categories like Wikipedia
      - `prop=categories&cllimit=500&clcategories=…` → `categories` in
        `query.pages.*`
          - note that `clcategories` takes at most 50 titles
          - be prepared to handle `query-continue.categories.clcontinue`,
            unless we limit `clcategories` to 10, in which case 50 `titles` ✕
            10 `clcategories` = 500 `cllimit`
      - as a side note, pages should ideally use a template defined at
        [[MediaWiki:Disambiguationspage]], but this doesn't happen consistently
        in practice to be relied upon
  - from Wikipedia, `prop=pageprops&ppprop=disambiguation` → `disambiguation`
    in `query.pages.*.pageprops`

For determining whether we can edit a page during `action=query`:
  - from Wikia, `prop=info&inprop=protection` → for protections in
    `query.pages.*.protection` where `type` is `edit`, compare against `level`
  - from Wikipedia, `prop=info&intestactions=edit` → `edit` in
    `query.pages.*.actions`
'''


UA_PRODUCT = 'Wikia!bokunoheroacademia!Puxbot'
SECS_BETWEEN_REQUESTS = 2
TITLES_PER_BATCH = 50
WIKIA_ENDPOINT_FMT = 'https://{0}.wikia.com/api.php'
WIKIA_SERVER_RE = re.compile(
    r'^https://(?P<name>[-.\w]+)\.(?:fandom|wikia)\.com$'
)
WIKIPEDIA_ENDPOINT_FMT = 'https://{0}.wikipedia.org/w/api.php'
WIKIPEDIA_SERVER_RE = re.compile(r'^//(?P<name>\w+)\.wikipedia\.org$')


class EditContext(NamedTuple):
    base_rid: int
    base_rts: datetime
    start_ts: datetime
    token: str


class _LRCStage(IntEnum):
    INITIALIZED = 0
    NAME_CANONICALIZED = 1
    TITLE_NORMALIZED = 2
    REDIRECTED = 3
    PAGE_CHECKED = 4
    SECTION_CHECKED = 5
    FINALIZED = 6


class _LRCState(IntEnum):
    PRISTINE = 0
    UPDATED = 1
    DROPPED_MISSING_PAGE = 2
    DROPPED_MISSING_SECTION = 3
    DROPPED_CONFLICTING_SECTIONS = 4
    DROPPED_REDIRECTION_LOOP = 5
    DROPPED_DISAMBIGUATION_PAGE = 6


class _LRChain:
    __slots__ = ('stage', 'state', 'from_target', 'to_target', 'desc')

    stage: _LRCStage
    state: _LRCState
    from_target: LinkTarget
    to_target: LinkTarget
    desc: str

    def __init__(self, target: LinkTarget) -> None:
        self.stage = _LRCStage.INITIALIZED
        self.state = _LRCState.PRISTINE
        self.from_target = target
        self.to_target = target
        self.desc = target.new_style_markup

    def __str__(self) -> str:
        return self.desc

    def canonicalize_name(self, name: str) -> None:
        assert ((self.stage == _LRCStage.INITIALIZED) and
                (self.state == _LRCState.PRISTINE) and
                (name != self.to_target.wiki_name))
        self.to_target = self.to_target._replace(wiki_name=name)
        self.desc = (self.to_target.new_style_markup +
                     f' (wiki name canonicalization) << {self.desc}')
        self.stage = _LRCStage.NAME_CANONICALIZED
        self.state = _LRCState.UPDATED

    def normalize_title(self, title: str) -> None:
        assert ((self.stage < _LRCStage.TITLE_NORMALIZED) and
                (self.state <= _LRCState.UPDATED) and
                (title != self.to_target.page_title))
        self.to_target = self.to_target._replace(page_title=title)
        self.desc = (self.to_target.new_style_markup +
                     f' (title normalization) << {self.desc}')
        self.stage = _LRCStage.TITLE_NORMALIZED
        self.state = _LRCState.UPDATED

    def redirect(self, title: str, section: str) -> None:
        assert ((self.stage <= _LRCStage.REDIRECTED) and
                (self.state <= _LRCState.UPDATED))
        self.to_target = self.to_target._replace(page_title=title,
                                                 page_section=section)
        self.desc = (self.to_target.new_style_markup +
                     f' (redirection) << {self.desc}')
        self.stage = _LRCStage.REDIRECTED
        self.state = _LRCState.UPDATED

    def drop_for_conflicting_sections(self) -> None:
        assert ((self.stage == _LRCStage.REDIRECTED) and
                (self.state == _LRCState.UPDATED))
        self.desc = f'(dropped: conflicting sections) << {self.desc}'
        self.stage = _LRCStage.FINALIZED
        self.state = _LRCState.DROPPED_CONFLICTING_SECTIONS

    def drop_for_redirection_loop(self) -> None:
        assert ((self.stage == _LRCStage.REDIRECTED) and
                (self.state == _LRCState.UPDATED))
        self.desc = f'(dropped: redirection loop) << {self.desc}'
        self.stage = _LRCStage.FINALIZED
        self.state = _LRCState.DROPPED_REDIRECTION_LOOP

    def check_page(self) -> None:
        assert ((self.stage < _LRCStage.PAGE_CHECKED) and
                (self.state <= _LRCState.UPDATED))
        self.stage = _LRCStage.PAGE_CHECKED

    def drop_for_missing_page(self) -> None:
        assert ((self.stage < _LRCStage.PAGE_CHECKED) and
                (self.state <= _LRCState.UPDATED))
        self.desc = f'(dropped: missing page) << {self.desc}'
        self.stage = _LRCStage.FINALIZED
        self.state = _LRCState.DROPPED_MISSING_PAGE

    def drop_for_disambiguation_page(self) -> None:
        assert ((self.stage < _LRCStage.PAGE_CHECKED) and
                (self.state <= _LRCState.UPDATED))
        self.desc = f'(dropped: disambiguation page) << {self.desc}'
        self.stage = _LRCStage.FINALIZED
        self.state = _LRCState.DROPPED_DISAMBIGUATION_PAGE

    def check_section(self) -> None:
        assert ((self.stage < _LRCStage.SECTION_CHECKED) and
                (self.state <= _LRCState.UPDATED))
        self.stage = _LRCStage.SECTION_CHECKED

    def drop_for_missing_section(self) -> None:
        assert ((self.stage < _LRCStage.SECTION_CHECKED) and
                (self.state <= _LRCState.UPDATED))
        self.desc = f'(dropped: missing section) << {self.desc}'
        self.stage = _LRCStage.FINALIZED
        self.state = _LRCState.DROPPED_MISSING_SECTION

    def finalize(self) -> None:
        assert self.stage != _LRCStage.FINALIZED
        if self.state == _LRCState.PRISTINE:
            self.desc = f'(noop) << {self.desc}'
        elif self.state == _LRCState.UPDATED:
            self.desc = f'(updated) << {self.desc}'
        else:
            assert False
        self.stage = _LRCStage.FINALIZED


class _LRStage(IntEnum):
    INITIALIZED = 0
    CHECKING_PAGES = 1
    PAGES_CHECKED = 2
    CHECKING_SECTIONS = 3
    SECTIONS_CHECKED = 4
    FINALIZED = 5


class LinkResolutionStats(NamedTuple):
    pristine: int
    updated: int
    dropped_missing_page: int
    dropped_missing_section: int
    dropped_conflicting_sections: int
    dropped_redirection_loop: int
    dropped_disambiguation_page: int

    def __add__(self, other: Tuple) -> 'LinkResolutionStats':
        return LinkResolutionStats(*(sum(pair) for pair in zip(self, other)))


class LinkResolutionGraph:
    __slots__ = ('pages_to_check', 'sections_to_check', 'stage',
                 'resolving_chains', 'finalized_chains', 'chains',
                 'inverse_mapping', 'stats', '_chains_by_title', '_titles')

    pages_to_check: ContextManager[Sequence[str]]
    sections_to_check: ContextManager[Sequence[str]]
    stage: _LRStage
    resolving_chains: Set[_LRChain]
    finalized_chains: Set[_LRChain]
    chains: Sequence[_LRChain]
    inverse_mapping: Mapping[LinkTarget, Set[LinkTarget]]
    stats: LinkResolutionStats
    _chains_by_title: Dict[str, Set[_LRChain]]
    _titles: Set[str]

    def __init__(self, wiki_project: WikiProject, wiki_name: str,
                 targets: Set[LinkTarget]) -> None:
        assert all(lt.wiki_project == wiki_project for lt in targets)
        self.pages_to_check = self._pages_to_check()
        self.sections_to_check = self._sections_to_check()
        self.stage = _LRStage.INITIALIZED
        self.resolving_chains = set()
        self.finalized_chains = set()
        for target in targets:
            chain = _LRChain(target)
            if target.wiki_name != wiki_name:
                chain.canonicalize_name(wiki_name)
            self.resolving_chains.add(chain)

    @contextmanager
    def _pages_to_check(self) -> Generator[Sequence[str], None, None]:
        assert self.stage == _LRStage.INITIALIZED
        self._chains_by_title = partition(self.resolving_chains,
                                          lambda c: c.to_target.page_title)
        self._titles = set()
        self.stage = _LRStage.CHECKING_PAGES
        yield sorted(self._chains_by_title.keys())
        assert ((self.stage == _LRStage.CHECKING_PAGES) and
                (not self._chains_by_title))
        self.stage = _LRStage.PAGES_CHECKED

    @contextmanager
    def _sections_to_check(self) -> Generator[Sequence[str], None, None]:
        assert ((self.stage == _LRStage.PAGES_CHECKED) and
                all(chain.to_target.page_section != ''
                    for chain in self.resolving_chains))
        self._chains_by_title = partition(self.resolving_chains,
                                          lambda c: c.to_target.page_title)
        self.stage = _LRStage.CHECKING_SECTIONS
        yield sorted(self._chains_by_title.keys())
        assert ((self.stage == _LRStage.CHECKING_SECTIONS) and
                (not self._chains_by_title) and
                (not self.resolving_chains))
        self.stage = _LRStage.SECTIONS_CHECKED

    def witness_normalization(self, from_title: str, to_title: str) -> None:
        assert self.stage == _LRStage.CHECKING_PAGES
        from_chains = self._chains_by_title[from_title]
        to_chains = self._chains_by_title.setdefault(to_title, set())
        for chain in from_chains:
            chain.normalize_title(to_title)
        del self._chains_by_title[from_title]
        to_chains.update(from_chains)

    def witness_redirection(self, from_title: str, to_title: str,
                            to_section: Optional[str]) -> None:
        assert self.stage == _LRStage.CHECKING_PAGES
        from_chains = self._chains_by_title[from_title]
        if to_section is not None:
            assert to_section != ''
            from_chains_with_conflicting_sections = set(
                chain for chain in from_chains if chain.to_target.page_section
            )
            for chain in from_chains_with_conflicting_sections:
                chain.redirect(to_title, to_section)
                chain.drop_for_conflicting_sections()
                self.resolving_chains.remove(chain)
                self.finalized_chains.add(chain)
            from_chains -= from_chains_with_conflicting_sections
        for chain in from_chains:
            chain.redirect(to_title,
                           to_section or chain.to_target.page_section)
        self._titles.add(from_title)
        if to_title in self._titles:
            assert ((from_title == to_title) or
                    (to_title not in self._chains_by_title))
            for chain in from_chains:
                # NB: Not all chains will log the complete redirection loop,
                #     but at least one will.
                chain.drop_for_redirection_loop()
                self.resolving_chains.remove(chain)
                self.finalized_chains.add(chain)
        else:
            to_chains = self._chains_by_title.setdefault(to_title, set())
            to_chains.update(from_chains)
        del self._chains_by_title[from_title]

    def witness_page(self, title: str, is_missing: bool,
                     is_disambiguation: bool) -> None:
        assert ((self.stage == _LRStage.CHECKING_PAGES) and
                (not (is_missing and is_disambiguation)))
        for chain in self._chains_by_title[title]:
            if is_missing:
                chain.drop_for_missing_page()
            elif is_disambiguation:
                chain.drop_for_disambiguation_page()
            else:
                chain.check_page()
                if chain.to_target.page_section != '':
                    continue
                chain.finalize()
            self.resolving_chains.remove(chain)
            self.finalized_chains.add(chain)
        del self._chains_by_title[title]

    def witness_sections(self, title: str, sections: Set[str]) -> None:
        assert self.stage == _LRStage.CHECKING_SECTIONS
        for chain in self._chains_by_title[title]:
            if chain.to_target.page_section.replace(' ', '_') in sections:
                chain.check_section()
                chain.finalize()
            else:
                chain.drop_for_missing_section()
            self.resolving_chains.remove(chain)
            self.finalized_chains.add(chain)
        del self._chains_by_title[title]

    def finalize(self) -> None:
        assert ((self.stage == _LRStage.SECTIONS_CHECKED) and
                (not self.resolving_chains) and
                all(chain.stage == _LRCStage.FINALIZED
                    for chain in self.finalized_chains))
        self.chains = sorted(self.finalized_chains,
                             key=(lambda c: (c.state, c.to_target,
                                             c.from_target)))
        self.inverse_mapping = dict()
        pristine = updated = dropped_missing_page = dropped_missing_section = \
            dropped_conflicting_sections = dropped_redirection_loop = \
            dropped_disambiguation_page = 0
        for chain in self.chains:
            if chain.state == _LRCState.PRISTINE:
                pristine += 1
            elif chain.state == _LRCState.UPDATED:
                updated += 1
            elif chain.state == _LRCState.DROPPED_MISSING_PAGE:
                dropped_missing_page += 1
            elif chain.state == _LRCState.DROPPED_MISSING_SECTION:
                dropped_missing_section += 1
            elif chain.state == _LRCState.DROPPED_CONFLICTING_SECTIONS:
                dropped_conflicting_sections += 1
            elif chain.state == _LRCState.DROPPED_REDIRECTION_LOOP:
                dropped_redirection_loop += 1
            elif chain.state == _LRCState.DROPPED_DISAMBIGUATION_PAGE:
                dropped_disambiguation_page += 1
            if chain.state <= _LRCState.UPDATED:
                bucket = self.inverse_mapping.setdefault(chain.to_target,
                                                         set())
                assert chain.from_target not in bucket
                bucket.add(chain.from_target)
        self.stats = LinkResolutionStats(
            pristine, updated, dropped_missing_page, dropped_missing_section,
            dropped_conflicting_sections, dropped_redirection_loop,
            dropped_disambiguation_page
        )
        self.stage = _LRStage.FINALIZED


class MediaWikiAPI:
    __slots__ = ('name', 'endpoint', 'session', 'last_req_finished',
                 'oneshot_req')

    PROJECT: WikiProject
    ENDPOINT_FMT: str
    SERVER_RE: Pattern
    name: str
    endpoint: str
    session: Session
    last_req_finished: float
    oneshot_req: Callable[['MediaWikiAPI', str, Mapping[str, str]],
                          Tuple[datetime, Mapping]]

    def __init__(self, name: str, task: str) -> None:
        self.name = name
        self.endpoint = self.ENDPOINT_FMT.format(name)

        self.session = Session()
        default_ua = self.session.headers['User-Agent']
        ua = f'{UA_PRODUCT} (interwiki-links-to-templates/{task}) {default_ua}'
        self.session.headers['User-Agent'] = ua

        self.last_req_finished = 0

        # NB: mypy can't quite grok Callable field assignments; see
        #     <https://github.com/python/mypy/issues/708>.
        self.oneshot_req = enforce_cooldown(  # type: ignore
            SECS_BETWEEN_REQUESTS, partial(getattr, self, 'last_req_finished'),
            partial(setattr, self, 'last_req_finished')
        )(self._oneshot_req)

    def _log(self, timestamp: datetime, msg: str) -> None:
        logger.info(msg, extra={'response_timestamp': timestamp})

    def _oneshot_req(self, action: str,
                     params: Mapping[str, str]) -> Tuple[datetime, Mapping]:
        finalized_params = {'format': 'json', 'action': action, **params}

        def prep_urlencoded_request(endpoint: str) -> PreparedRequest:
            return self.session.prepare_request(Request(
                'POST', endpoint, data=finalized_params
            ))

        def prep_multipart_request(endpoint: str) -> PreparedRequest:
            return self.session.prepare_request(Request(
                'POST', endpoint, files={
                    k: (None, v) for (k, v) in finalized_params.items()
                }
            ))

        def request_size(request: PreparedRequest) -> int:
            assert request.body is not None  # Appease mypy.
            # The other headers _should_ be identical between both requests.
            return (len(request.headers['Content-Length']) +
                    len(request.headers['Content-Type']) +
                    len(request.body))

        def post(request: PreparedRequest,
                 prep_request: Callable[[str], PreparedRequest]) -> Response:
            while True:
                response = self.session.send(request, allow_redirects=False)
                if response.status_code in (301, 302, 307, 308):
                    # If the wiki name isn't canonical, chances are we'll get a
                    # 301. Unfortunately, only 307s and 308s are guaranteed by
                    # spec not to mutilate our POST into a GET.
                    request = prep_request(response.next.url)
                    continue
                return response

        urlencoded_request = prep_urlencoded_request(self.endpoint)
        multipart_request = prep_multipart_request(self.endpoint)
        if request_size(urlencoded_request) <= request_size(multipart_request):
            response = post(urlencoded_request, prep_urlencoded_request)
        else:
            response = post(multipart_request, prep_multipart_request)

        assert response.status_code == 200
        json = response.json()
        assert all(key not in json.keys() for key in (
            'errors', 'error', 'warnings', 'continue', 'query-continue'
        ))
        results = json[action]
        assert isinstance(results, dict)
        return (self._oneshot_req_extract_timestamp(response), results)

    def _oneshot_req_extract_timestamp(self, response: Response) -> datetime:
        raise NotImplementedError

    def canonicalize(self) -> None:
        (timestamp, results) = self.oneshot_req('query', {
            'meta': 'siteinfo',
            'siprop': 'general',
        })
        match = self.SERVER_RE.fullmatch(results['general']['server'])
        assert match is not None

        name = self.name
        canonical_name = match.group('name')

        msg = 'Canonicalized wiki name: '
        if canonical_name == self.name:
            msg += '(noop)'
        else:
            msg += repr(canonical_name)
            self.name = canonical_name
            self.endpoint = self.ENDPOINT_FMT.format(canonical_name)
        msg += f' << {name!r}'
        self._log(timestamp, msg)

    def resolve_links(self, targets: Set[LinkTarget]) -> LinkResolutionGraph:
        graph = LinkResolutionGraph(self.PROJECT, self.name, targets)
        with graph.pages_to_check as titles:
            batches = list(zip(count(1), (
                titles[i:i+TITLES_PER_BATCH]
                for i in range(0, len(titles), TITLES_PER_BATCH)
            )))
            self._resolve_pages(batches, graph)
        with graph.sections_to_check as titles:
            for title in titles:
                sections = self.enumerate_page_sections(title)
                graph.witness_sections(title, sections)
        graph.finalize()
        return graph

    def _resolve_pages(self, batches: Sequence[Tuple[int, Sequence[str]]],
                       graph: LinkResolutionGraph) -> None:
        raise NotImplementedError

    def enumerate_page_sections(self, title: str) -> Set[str]:
        assert is_valid_page_title(title)
        (timestamp, results) = self.oneshot_req('parse', {
            'page': title,
            'prop': 'sections',
        })
        assert results['title'] == title
        anchors = set(section['anchor'] for section in results['sections'])
        self._log(timestamp, f'Enumerated page sections for [[{title}]]')
        return anchors


class WikiaAPI(MediaWikiAPI):
    __slots__ = ()

    PROJECT = WikiProject.WIKIA
    ENDPOINT_FMT = WIKIA_ENDPOINT_FMT
    SERVER_RE = WIKIA_SERVER_RE

    def _oneshot_req_extract_timestamp(self, response: Response) -> datetime:
        return rfc822_to_dt(response.headers['Date'])

    def _resolve_pages(self, batches: Sequence[Tuple[int, Sequence[str]]],
                       graph: LinkResolutionGraph) -> None:
        (timestamp, results) = self.oneshot_req('query', {
            'list': 'allcategories',
            'acprefix': 'Disambig',
            'aclimit': '10',
        })
        categories = sorted(x['*'] for x in results['allcategories'])
        if len(categories) > 0:
            self._log(timestamp, 'Identified the following disambiguation '
                                 f'categories: {categories!r}')
            disambiguation_params = {
                'prop': 'categories',
                'cllimit': '500',
                'clcategories': '|'.join(f'Category:{category}'
                                         for category in categories),
            }
        else:
            self._log(timestamp, 'Identified no disambiguation categories')
            disambiguation_params = dict()

        num_batches = len(batches)
        for (batch_num, titles) in batches:
            (timestamp, results) = self.oneshot_req('query', {
                'titles': '|'.join(titles),
                **disambiguation_params,
                'redirects': '',
            })
            normalizations = results.get('normalized', [])
            redirections = results.get('redirects', [])
            pages = results.get('pages', {}).values()
            self._log(timestamp, f'Queried batch {batch_num} of {num_batches} '
                                 f'(from {titles[0]!r} to {titles[-1]!r})')
            for x in normalizations:
                graph.witness_normalization(x['from'], x['to'])
            for x in redirections:
                graph.witness_redirection(x['from'], x['to'],
                                          x.get('tofragment'))
            for x in pages:
                graph.witness_page(x['title'], 'missing' in x,
                                   'categories' in x)

    def login(self, username: str, password: str) -> None:
        (timestamp, results) = self.oneshot_req('login', {'lgname': username})
        assert results['result'] == 'NeedToken'
        login_token = results['token']
        self._log(timestamp, 'Fetched login token')

        (timestamp, results) = self.oneshot_req('login', {
            'lgname': username,
            'lgpassword': password,
            'lgtoken': login_token,
        })
        assert results['result'] == 'Success'
        assert results['lgusername'] == username
        self._log(timestamp, 'Logged in')

        (timestamp, results) = self.oneshot_req('query', {
            'meta': 'userinfo',
            'uiprop': 'groups',
        })
        assert results['userinfo']['name'] == username
        assert 'bot' in results['userinfo']['groups']
        self._log(timestamp, 'Verified user is a bot')

    def query_page(self, title: str) -> Optional[Tuple[Page,
                                                       Optional[EditContext]]]:
        assert is_valid_page_title(title)
        (timestamp, results) = self.oneshot_req('query', {
            'titles': title,
            'prop': 'info|revisions',
            'inprop': 'protection',
            'intoken': 'edit',
            'rvprop': 'ids|timestamp|content',
        })
        assert len(results['pages']) == 1
        (pid, pinfo) = results['pages'].popitem()
        typed_pid = int(pid)
        msg = f'Queried [[{title}]]; '

        if typed_pid < 0:
            msg += 'title not found'
            self._log(timestamp, msg)
            return None

        assert typed_pid != 0
        assert pinfo['pageid'] == typed_pid
        assert pinfo['title'] == title  # Title shouldn't need normalizing.

        assert len(pinfo['revisions']) == 1
        typed_rid = pinfo['revisions'][0]['revid']
        assert ((typed_rid > 0) and (typed_rid == pinfo['lastrevid']))
        typed_rts = iso8601_to_dt(pinfo['revisions'][0]['timestamp'])
        text = pinfo['revisions'][0]['*']
        assert len(text.encode()) == pinfo['length']
        page = Page(typed_pid, typed_rid, typed_rts, title, text)
        msg += f'got [page ID = {typed_pid}; revision ID = {typed_rid}]; '

        # As far as Puxbot is concerned, this predicate is sufficient.
        editable = not any(((prot['type'] == 'edit') and
                            (prot['level'] == 'sysop'))
                           for prot in pinfo['protection'])

        if not editable:
            msg += 'not editable'
            self._log(timestamp, msg)
            return (page, None)

        edit_context = EditContext(typed_rid, typed_rts,
                                   iso8601_to_dt(pinfo['starttimestamp']),
                                   pinfo['edittoken'])
        msg += f'editable as at {dt_to_iso8601(edit_context.start_ts)}'
        self._log(timestamp, msg)
        return (page, edit_context)

    def edit_page(self, page: Page, edit_context: EditContext,
                  summary: str, is_minor: bool) -> Page:
        assert ((page.rid is None) and (page.rts is None))
        assert edit_context.base_rts <= edit_context.start_ts
        assert len(summary) > 0
        (timestamp, results) = self.oneshot_req('edit', {
            'title': page.title,
            'text': page.text,
            'summary': summary,
            **({'minor': ''} if is_minor else {}),
            'bot': '',
            'basetimestamp': dt_to_iso8601(edit_context.base_rts),
            'starttimestamp': dt_to_iso8601(edit_context.start_ts),
            'nocreate': '',
            'md5': md5(page.text.encode()).hexdigest(),
            'token': edit_context.token,
        })
        assert results['result'] == 'Success'
        assert results['pageid'] == page.pid
        assert results['title'] == page.title
        # I don't trust edit conflict resolution, so let's just race to be
        # first to edit. ¯\_(ツ)_/¯
        assert results['oldrevid'] == edit_context.base_rid
        typed_rid = results['newrevid']
        assert typed_rid > 0
        typed_rts = iso8601_to_dt(results['newtimestamp'])
        assert typed_rts >= edit_context.start_ts
        self._log(timestamp, f'Saved [[{page.title}]] as [page ID = '
                             f'{page.pid}; revision ID = {typed_rid}]')

        (timestamp, results) = self.oneshot_req('query', {
            'revids': str(typed_rid),
            'prop': 'revisions',
            'rvprop': 'content',
        })
        assert len(results['pages']) == 1
        pinfo = results['pages'][str(page.pid)]
        assert pinfo['pageid'] == page.pid
        assert pinfo['title'] == page.title
        self._log(timestamp, f'Queried [[{page.title}]] at [page ID = '
                             f'{page.pid}; revision ID = {typed_rid}]')
        return Page(page.pid, typed_rid, typed_rts, page.title,
                    pinfo['revisions'][0]['*'])


class WikipediaAPI(MediaWikiAPI):
    __slots__ = ()

    PROJECT = WikiProject.WIKIPEDIA
    ENDPOINT_FMT = WIKIPEDIA_ENDPOINT_FMT
    SERVER_RE = WIKIPEDIA_SERVER_RE

    def _oneshot_req(self, action: str,
                     params: Mapping[str, str]) -> Tuple[datetime, Mapping]:
        return super()._oneshot_req(action, {**params, 'curtimestamp': ''})

    def _oneshot_req_extract_timestamp(self, response: Response) -> datetime:
        return iso8601_to_dt(response.json()['curtimestamp'])

    def _resolve_pages(self, batches: Sequence[Tuple[int, Sequence[str]]],
                       graph: LinkResolutionGraph) -> None:
        num_batches = len(batches)
        for (batch_num, titles) in batches:
            (timestamp, results) = self.oneshot_req('query', {
                'titles': '|'.join(titles),
                'prop': 'pageprops',
                'ppprop': 'disambiguation',
                'redirects': '',
            })
            normalizations = results.get('normalized', [])
            redirections = results.get('redirects', [])
            pages = results.get('pages', {}).values()
            self._log(timestamp, f'Queried batch {batch_num} of {num_batches} '
                                 f'(from {titles[0]!r} to {titles[-1]!r})')
            for x in normalizations:
                graph.witness_normalization(x['from'], x['to'])
            for x in redirections:
                graph.witness_redirection(x['from'], x['to'],
                                          x.get('tofragment'))
            for x in pages:
                graph.witness_page(x['title'], 'missing' in x,
                                   'pageprops' in x)


def mwapi(project: WikiProject, name: str, task: str) -> MediaWikiAPI:
    if project == WikiProject.WIKIA:
        return WikiaAPI(name, task)
    elif project == WikiProject.WIKIPEDIA:
        return WikipediaAPI(name, task)
    assert False
