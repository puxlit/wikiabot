#!/usr/bin/env python3.7

from getpass import getpass
import sys

from common import (SubstEffect, diff_pages, eprint, eprint_stats,
                    find_interwiki_links, load_candidate_pages,
                    load_remapped_links, openrt, openwt,
                    replace_interwiki_links)
from mwapi import WikiaAPI, prepare_mwapi_logging

__all__ = ('main',)


##############################################################################

USAGE_MSG = ('usage: {0} candidate_pages.txt remapped_links.txt '
             'prepared_edits.patch committed_edits.patch '
             'uncommitted_edits.patch')
BOT_USERNAME = 'Puxbot'
EDIT_SUMMARY = 'Update interwiki links'
STATS_MSG = '''\
----------------
Out of      {0} candidate page{1},
examined    {2} page{3} and
found       {4} IWL{5}
across      {6} page{7}.

Ignored     {8} missing page{9},
skipped     {10} page{11} without IWLs and
            {12} unchanged page{13},
encountered {14} uneditable page{15}, and
changed     {16} editable page{17}.

Found       {18} already optimal IWL{19},
changed     {20} IWL{21}, and
skipped     {22} unmapped IWL{23}.'''


def main() -> None:
    if len(sys.argv) != 6:
        eprint(USAGE_MSG.format(sys.argv[0]))
        sys.exit(1)

    candidate_pages_if = openrt(sys.argv[1])
    remapped_links_if = openrt(sys.argv[2])
    prepared_edits_of = openwt(sys.argv[3])
    committed_edits_of = openwt(sys.argv[4])
    uncommitted_edits_of = openwt(sys.argv[5])

    titles = load_candidate_pages(candidate_pages_if)
    mapping = load_remapped_links(remapped_links_if)

    num_examined_pages = 0
    num_matched_iwls = 0
    num_matched_pages = 0
    num_missing_pages = 0
    num_unmatched_pages = 0
    num_unchanged_pages = 0
    num_uneditable_pages = 0
    num_changed_pages = 0
    prepared_diffs = []
    committed_diffs = []
    uncommitted_diffs = []
    num_noop_iwls = 0
    num_replaced_iwls = 0
    num_skipped_iwls = 0

    prepare_mwapi_logging(0)
    api = WikiaAPI('bokunoheroacademia', 'perform_mass_edit')
    api.login(BOT_USERNAME, getpass())
    for title in sorted(titles):
        query_result = api.query_page(title)
        if query_result is None:
            num_missing_pages += 1
            continue
        num_examined_pages += 1
        (a_page, edit_context) = query_result
        matches = find_interwiki_links(a_page)
        if len(matches) == 0:
            num_unmatched_pages += 1
            continue
        num_matched_iwls += len(matches)
        num_matched_pages += 1

        eprint(f'  - for page [[{a_page.title}]]:')

        page_has_changed = False
        (b_page, substs) = replace_interwiki_links(a_page, matches, mapping)
        assert len(substs) == len(matches)
        for ((a_start, a_end, a_markup, a_link),
             (b_start, b_end, b_markup, b_link), effect) in substs:
            msg = f'      - @ ({b_start},{b_end}) << ({a_start},{a_end}): '
            if effect == SubstEffect.NOOP:
                num_noop_iwls += 1
                msg += '(noop)'
            elif effect == SubstEffect.REPLACED:
                page_has_changed = True
                num_replaced_iwls += 1
                msg += repr(b_markup)
            elif effect == SubstEffect.SKIPPED:
                num_skipped_iwls += 1
                msg += '(skipped)'
            msg += f' << {a_markup!r}'
            eprint(msg)

        prepared_diff = diff_pages(a_page, b_page)
        prepared_diffs.append(prepared_diff)
        if not page_has_changed:
            num_unchanged_pages += 1
            continue
        if edit_context is None:
            uncommitted_diffs.append(prepared_diff)
            num_uneditable_pages += 1
            continue
        c_page = api.edit_page(b_page, edit_context, EDIT_SUMMARY, True)
        committed_diffs.append(diff_pages(a_page, c_page))
        num_changed_pages += 1

    with prepared_edits_of:
        for diff in prepared_diffs:
            prepared_edits_of.write(diff)
    with committed_edits_of:
        for diff in committed_diffs:
            committed_edits_of.write(diff)
    with uncommitted_edits_of:
        for diff in uncommitted_diffs:
            uncommitted_edits_of.write(diff)

    num_candidate_pages = len(titles)
    eprint_stats(STATS_MSG, num_candidate_pages, num_examined_pages,
                 num_matched_iwls, num_matched_pages, num_missing_pages,
                 num_unmatched_pages, num_unchanged_pages,
                 num_uneditable_pages, num_changed_pages, num_noop_iwls,
                 num_replaced_iwls, num_skipped_iwls)


##############################################################################

if __name__ == '__main__':
    main()
