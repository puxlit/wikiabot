#!/usr/bin/env python3.7

import sys

from common import (LinkStyle, eprint, eprint_stats, find_interwiki_links,
                    main_pages_from_dump, openrt, openwt, save_candidate_pages,
                    save_interwiki_links)

__all__ = ('main',)


##############################################################################

USAGE_MSG = ('usage: {0} pages_current.xml candidate_pages.txt '
             'interwiki_links.txt')
STATS_MSG = '''\
----------------
Examined {0} page{1} in the main namespace.

Found    {2} page{3} with interwiki links (IWLs),
         {4} IWL{5} using preferred template markup,
         {6} IWL{7} using regular link markup, and
         {8} unique IWL target{9}.'''


def main() -> None:
    if len(sys.argv) != 4:
        eprint(USAGE_MSG.format(sys.argv[0]))
        sys.exit(1)

    pages_current_if = openrt(sys.argv[1])
    candidate_pages_of = openwt(sys.argv[2])
    interwiki_links_of = openwt(sys.argv[3])

    num_pages = 0
    titles = set()
    num_new_style = 0
    num_old_style = 0
    targets = set()

    with pages_current_if:
        for page in main_pages_from_dump(pages_current_if):
            num_pages += 1
            results = find_interwiki_links(page)
            if len(results) == 0:
                continue
            titles.add(page.title)

            eprint(f'  - for page [[{page.title}]]:')

            for (start, end, markup, link) in results:
                if link.style == LinkStyle.NEW:
                    num_new_style += 1
                    msg = '      - new style '
                elif link.style == LinkStyle.OLD:
                    num_old_style += 1
                    msg = '      - old style '
                targets.add(link.target)

                flags = []
                if link.target.is_section_link:
                    flags.append('IS SECTION LINK')
                if link.has_significant_label:
                    flags.append('HAS SIGNIFICANT LABEL')
                if len(flags) > 0:
                    msg += '/* ' + '; '.join(flags) + ' */ '
                msg += f'@ ({start},{end}): {markup!r}'
                eprint(msg)

    save_candidate_pages(candidate_pages_of, titles)
    save_interwiki_links(interwiki_links_of, targets)

    num_candidate_pages = len(titles)
    num_targets = len(targets)
    eprint_stats(STATS_MSG, num_pages, num_candidate_pages,
                 num_new_style, num_old_style, num_targets)


##############################################################################

if __name__ == '__main__':
    main()
